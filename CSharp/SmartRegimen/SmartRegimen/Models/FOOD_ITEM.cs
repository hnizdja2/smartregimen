//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartRegimen.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FOOD_ITEM
    {
        public FOOD_ITEM()
        {
            this.FOOD_ITEM_IN_FOOD = new HashSet<FOOD_ITEM_IN_FOOD>();
            this.PRICEs = new HashSet<PRICE>();
        }
    
        public int FOIT_ID { get; set; }
        public int FOIT_NAME_TEXT_ID { get; set; }
        public double FOIT_SUGAR { get; set; }
        public double FOIT_FAT { get; set; }
        public double FOIT_PROTEIN { get; set; }
        public int FOIT_CATE_ID { get; set; }
        public double FOIT_ENERGY { get; set; }
    
        public virtual CATEGORY CATEGORY { get; set; }
        public virtual ICollection<FOOD_ITEM_IN_FOOD> FOOD_ITEM_IN_FOOD { get; set; }
        public virtual TEXT TEXT { get; set; }
        public virtual ICollection<PRICE> PRICEs { get; set; }
    }
}
