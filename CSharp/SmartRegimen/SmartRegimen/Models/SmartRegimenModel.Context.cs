﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartRegimen.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class SmartRegimenEntities : DbContext
    {
        public SmartRegimenEntities()
            : base("name=SmartRegimenEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<CATEGORY> CATEGORies { get; set; }
        public virtual DbSet<DIET> DIETs { get; set; }
        public virtual DbSet<DIET_DAY> DIET_DAY { get; set; }
        public virtual DbSet<FOOD> FOODs { get; set; }
        public virtual DbSet<FOOD_IN_DIET_DAY> FOOD_IN_DIET_DAY { get; set; }
        public virtual DbSet<FOOD_ITEM> FOOD_ITEM { get; set; }
        public virtual DbSet<FOOD_ITEM_IN_FOOD> FOOD_ITEM_IN_FOOD { get; set; }
        public virtual DbSet<FOOD_TYPE> FOOD_TYPE { get; set; }
        public virtual DbSet<LANGUAGE> LANGUAGEs { get; set; }
        public virtual DbSet<PERSON> People { get; set; }
        public virtual DbSet<SPICE> SPICEs { get; set; }
        public virtual DbSet<TEXT> TEXTs { get; set; }
        public virtual DbSet<CURRENCY> CURRENCies { get; set; }
        public virtual DbSet<STORE> STOREs { get; set; }
        public virtual DbSet<PRICE> PRICEs { get; set; }
    
        [DbFunction("SmartRegimenEntities", "GetDailyFood")]
        public virtual IQueryable<GetDailyFood_Result> GetDailyFood(string input)
        {
            var inputParameter = input != null ?
                new ObjectParameter("input", input) :
                new ObjectParameter("input", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<GetDailyFood_Result>("[SmartRegimenEntities].[GetDailyFood](@input)", inputParameter);
        }
    
        [DbFunction("SmartRegimenEntities", "GetDailySpices")]
        public virtual IQueryable<string> GetDailySpices(string input)
        {
            var inputParameter = input != null ?
                new ObjectParameter("input", input) :
                new ObjectParameter("input", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<string>("[SmartRegimenEntities].[GetDailySpices](@input)", inputParameter);
        }
    
        [DbFunction("SmartRegimenEntities", "GetMatchingFoodItems")]
        public virtual IQueryable<GetMatchingFoodItems_Result> GetMatchingFoodItems(string input)
        {
            var inputParameter = input != null ?
                new ObjectParameter("input", input) :
                new ObjectParameter("input", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<GetMatchingFoodItems_Result>("[SmartRegimenEntities].[GetMatchingFoodItems](@input)", inputParameter);
        }
    }
}
