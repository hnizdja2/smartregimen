//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SmartRegimen.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LANGUAGE
    {
        public LANGUAGE()
        {
            this.People = new HashSet<PERSON>();
            this.TEXTs = new HashSet<TEXT>();
        }
    
        public int LANG_ID { get; set; }
        public string LANG_NAME { get; set; }
    
        public virtual ICollection<PERSON> People { get; set; }
        public virtual ICollection<TEXT> TEXTs { get; set; }
    }
}
