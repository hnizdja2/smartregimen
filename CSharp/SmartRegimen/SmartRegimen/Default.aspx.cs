﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartRegimen.Models;
using SmartRegimen.Classes;

namespace SmartRegimen
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var data = new SmartRegimenEntities()) {
                var q = data.FOOD_ITEM.Join(data.TEXTs, food => food.FOIT_NAME_TEXT_ID, text => text.TEXT_ID, (food, text) => new { food.FOIT_ID, text.TEXT_CONTENT });
                GridView1.DataSource = q.ToList();
                GridView1.DataBind();
            }

            using (var data = new SmartRegimenEntities()) {
                var q = data.SPICEs.Join(data.TEXTs, spice => spice.SPIC_NAME_TEXT_ID, text => text.TEXT_ID, (spice, text) => new { spice.SPIC_ID, text.TEXT_CONTENT });
                GridView2.DataSource = q.ToList();
                GridView2.DataBind();
            }

            //DietImporter.createSimonDiet();
            //DietImporter.createHonzaDiet();
            //DietImporter.createPrices();
        }
    }
}