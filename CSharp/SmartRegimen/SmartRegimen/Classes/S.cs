﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartRegimen.Classes
{
    public class S
    {
        public static int BAZALKA = 1;
        public static int BYLINKY = 8;
        public static int CESNEK = 3;
        public static int CHILLI = 13;
        public static int CITRON = 19;
        //public static int CITRONOVA_STAVA = 9;
        public static int CUKR = 33;
        public static int GRILOVACI_KORENI = 25;
        public static int HORCICE = 26;
        public static int KAKAO = 17;
        public static int KARI_KORENI = 29;
        public static int KMIN = 22;
        public static int KORENI = 10;
        public static int KORENI_NA_CINU = 30;
        public static int MEDUNKA = 20;
        public static int MLETA_CERVENA_PAPRIKA = 24;
        public static int OCET = 11;
        public static int OCET_BALZAMICO = 2;
        public static int OLEJ = 32;
        public static int OREGANO = 21;
        public static int PALIVA_PAPRIKA = 12;
        public static int PEPR = 7;
        public static int PETRZEL = 23;
        public static int PROVENSALSKE_KORENI = 4;
        public static int SKORICE = 16;
        public static int SOJOVA_OMACKA = 18;
        public static int SPETKA_CUKRU = 6;
        public static int SUL = 5;
        public static int TYMIAN = 27;
        public static int VINNY_OCET = 28;
        public static int VODA = 31;
        public static int ZELENINOVE_KORENI = 15;
    }
}