﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartRegimen.Models;
using SmartRegimen.Classes;

namespace SmartRegimen
{
    public class DietImporter
    {
        //SIMON'S DIET
        public static void createSimonDiet()
        {
            using (var data = new SmartRegimenEntities())
            {
                //DO NOT ALTER
                DIET_DAY Dt;
                FOOD Ft;

                //DIET
                DIET diet = new DIET();
                diet.TEXT_DESC = addText("Gain Diet");
                diet.PERSON = addPerson("Šimon", "Hlaváč");
                diet.TEXT_NAME = addText("Eat Well Gain Diet");

                //DAY p1 in v2
                Dt = addDay(diet, "Day 1");
                Ft = addFood(Dt, T.BREAKFAST, "Chleba s lučinou a hermelínem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 130);
                addItem(Ft, F.LUCINA_LINIE, 30);
                addItem(Ft, F.HERMELIN_FIGURA_SEDLCANSKY, 35);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.MANDLE, 20);

                Ft = addFood(Dt, T.MORNING_SNACK, "Pečivo s lučinou", null, null, null);

                addItem(Ft, F.MANDARINKA, 80);
                addItem(Ft, F.CELOZRNNE_PECIVO, 65);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.MANDLE, 6);

                Ft = addFood(Dt, T.LUNCH, "Bulgur na cibulce", "Cibuli a část zeleniny orestujeme na oleji, osolíme, opepříme a promícháme s bulgurem uvařeným v mírně slané vodě. Kousky rybího masa okořeníme, orestujeme na zbytku oleje do měkka a teplé je vmícháme do bulguru. Dle chuti dokořeníme a podáváme se zeleninovým salátem.", true, null);

                addItem(Ft, F.FILE_TRESKA, 120);
                addItem(Ft, F.BULGUR, 100);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 20);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.GRILOVACI_KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s tvarohem", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 90);
                addItem(Ft, F.MANDARINKA, 150);
                addItem(Ft, F.OVESNE_VLOCKY, 35);
                addItem(Ft, F.SEMINKA, 8);

                Ft = addFood(Dt, T.DINNER, "Zapečená brokolice se sýrem", null, null, null);

                addItem(Ft, F.BROKOLICE, 250);
                addItem(Ft, F.MADELAND_FITNESS_10P_MADETA, 55);
                addItem(Ft, F.CELOZRNNE_PECIVO, 80);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);


                //DAY p2 in v2
                Dt = addDay(diet, "Day 2");
                Ft = addFood(Dt, T.BREAKFAST, "Vločky s vínem", null, null, null);

                addItem(Ft, F.OVESNE_VLOCKY, 70);
                addItem(Ft, F.TVAROH_PLNOTUCNY, 100);
                addItem(Ft, F.HROZNOVE_VINO, 80);
                addItem(Ft, F.SEMINKA, 10);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);

                Ft = addFood(Dt, T.MORNING_SNACK, "Lifebar s ovocem, chlebem a lučinou", null, null, null);

                addItem(Ft, F.HRUSKA, 120);
                addItem(Ft, F.TYCINKA_LIFEBAR_FIKOVA_LIFEFOOD, 23);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 50);
                addItem(Ft, F.LUCINA_LINIE, 20);

                Ft = addFood(Dt, T.LUNCH, "Sýrové těstoviny se špenátem", "Těstoviny uvaříme a scedíme. Jemně pokrájenou cibuli osmažíme na trošce oleje, přidáme nasekaný česnek, spařený špenát a pokrájená rajčata (i se šťávou). Ochutíme kořením a solí (případně přilijeme trochu vařící vody na zředění zeleninové omáčky). Těstoviny promícháme s hotovou omáčkou a posypeme nastrouhaným sýrem.", true, null);

                addItem(Ft, F.CELOZRNNE_TESTOVINY, 120);
                addItem(Ft, F.MADELAND_LIGHT_30_P, 50);
                addItem(Ft, F.SPENATOVE_LISTY, 50);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.RAJCATA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 10);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.PROVENSALSKE_KORENI);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CUKR);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s vínem", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.HROZNOVE_VINO, 100);
                addItem(Ft, F.OVESNE_VLOCKY, 20);
                addItem(Ft, F.MANDLE, 8);
                addItem(Ft, F.PSENICNE_OTRUBY, 3);
                addItem(Ft, F.PSENICNE_KLICKY, 2);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);

                Ft = addFood(Dt, T.DINNER, "Krůtí steak s dušenou zeleninou", null, true, null);

                addItem(Ft, F.KRUTI_PRSA, 120);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.CELOZRNNE_PECIVO, 70);
                addItem(Ft, F.OLIVOVY_OLEJ, 7);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);

                //DAY p3 in v2
                Dt = addDay(diet, "Day 3");
                Ft = addFood(Dt, T.BREAKFAST, "Bageta s tvarohem", null, null, null);

                addItem(Ft, F.CELOZRNNA_BAGETA, 95);
                addItem(Ft, F.SVACINKA_ZELENINOVY_TVAROH_MILKO, 30);
                addItem(Ft, F.MADELAND_LIGHT_30_P, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.SEMINKA, 22);

                Ft = addFood(Dt, T.MORNING_SNACK, "Knackebrot s lučinou a tyčinkou", null, null, null);

                addItem(Ft, F.TYCINKA_LIFEBAR_FIKOVA_LIFEFOOD, 23);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.MANDARINKA, 160);

                Ft = addFood(Dt, T.LUNCH, "Sendvič s cizrnovým salátem", "Pečivo namažeme lučinou a obložíme plátky šunky. Salát připravíme z nakrájené zeleniny, slité cizrny, dochutíme kořením, octem balzamico a zakápneme olejem.", null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 115);
                addItem(Ft, F.LUCINA_LINIE, 30);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.CIZRNA_VE_SKLE_ALNATURA, 105);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.OCET_BALZAMICO);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Knackebrot s tyčinkou", null, null, null);

                addItem(Ft, F.PROTEINOVA_TYCINKA_SPORTNESS_32P_DM_DROGERIE_COOKIES, 45);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.DINNER, "Čína s krevetami", "Krevety osmahneme na oleji společně s většími kousky cibule. Přidáme nakrájenou zeleninu a podlijeme trochou vody, aby změkla. Směs ochutíme kořením podle potřeby. Během dušení uvaříme rýži, kterou přimícháme do směsi.", true, null);

                addItem(Ft, F.KREVETY, 180);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.RYZE_JASMINOVA, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 10);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI_NA_CINU);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.CHILLI);
         
                //DAY p4 in v2
                Dt = addDay(diet, "Day 4");
                Ft = addFood(Dt, T.BREAKFAST, "Pohanková kaše", "Pohankovou krupici stačí spařit vroucí vodou a nechat chvíli nabobtnat. Poté přidáme ostatní ingredience.", null, null);

                addItem(Ft, F.KRUPICE_POHANKOVA, 50);
                addItem(Ft, F.BANAN, 100);
                addItem(Ft, F.TVAROH_PLNOTUCNY, 150);
                addItem(Ft, F.MANDLE, 8);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb s banánem", null, null, null);

                addItem(Ft, F.BANAN, 120);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 50);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.MANDLE, 8);

                Ft = addFood(Dt, T.LUNCH, "Pečené kuřecí řízky", "Kuřecí prsa osolíme a potřeme utřeným česnekem. Okořeníme dle chuti a vložíme do olejem vymazaného pekáče. Přidáme zbytek česneku nakrájeného na plátky a upečeme v troubě doměkka. Podáváme s vařenými bramborami a zeleninovým salátem.", null, null);

                addItem(Ft, F.KURECI_PRSA, 150);
                addItem(Ft, F.OLIVOVY_OLEJ, 18);
                addItem(Ft, F.BRAMBORY, 350);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.CESNEK);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s banánem", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.BANAN, 100);
                addItem(Ft, F.OVESNE_VLOCKY, 20);
                addItem(Ft, F.SEMINKA, 8);
                addItem(Ft, F.PSENICNE_OTRUBY, 3);
                addItem(Ft, F.PSENICNE_KLICKY, 2);

                Ft = addFood(Dt, T.DINNER, "Pečivo se šmakounem", "Šmakouna nakrájíme na kousky a lehce osmahneme na nepřilnavé pánvi. Přidáme k čerstvé zelenině a nakrájenému hermelínu. Okořeníme a zalijeme bílým jogurtem. Podáváme s pečivem namazaným lučinou.", null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 65);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SMAKOUN_ORIGINAL, 150);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.HERMELIN_FIGURA_SEDLCANSKY, 30);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 60);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);

                //DAY p5 in v2
                Dt = addDay(diet, "Day 5");
                Ft = addFood(Dt, T.BREAKFAST, "Chléb s lososem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 140);
                addItem(Ft, F.MASLO, 10);
                addItem(Ft, F.UZENY_LOSOS_NORSKE_PLATKY, 45);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.MANDLE, 14);

                Ft = addFood(Dt, T.MORNING_SNACK, "Pečivo s lučinou a pomerančem", null, null, null);

                addItem(Ft, F.POMERANC, 170);
                addItem(Ft, F.CELOZRNNE_PECIVO, 50);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SEMINKA, 7);

                Ft = addFood(Dt, T.LUNCH, "Hovězí maso s fazolkami", "Na oleji necháme zesklovatět cibuli, poté přidáme fazolové lusky, vše osolíme, opepříme a chvilku podusíme (ke konci přidáme lisovaný česnek). Hovězí maso okořeníme a orestujeme do měkka. Podáváme s vařenými bramborami a zeleninovou oblohou.", null, null);

                addItem(Ft, F.HOVEZI_SVICKOVA, 160);
                addItem(Ft, F.FAZOLOVE_LUSKY_MRAZENE_BONDUELLE, 80);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 10);
                addItem(Ft, F.BRAMBORY, 300);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.CHILLI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chleba s šunkou a tvarohem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 80);
                addItem(Ft, F.SVACINKA_ZELENINOVY_TVAROH_MILKO, 50);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 20);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.SEMINKA, 15);

                Ft = addFood(Dt, T.DINNER, "Kuřecí kousky", null, null, null);

                addItem(Ft, F.KURECI_PRSA, 140);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 105);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.OLIVOVY_OLEJ, 7);

                //DAY p6 in v2
                Dt = addDay(diet, "Day 6");
                Ft = addFood(Dt, T.BREAKFAST, "Míchaná vejce", "Na rozpáleném oleji rozmícháme vejce s bílky, okořeníme a posypeme nakrájeným sýrem. Podáváme s čerstvou zeleninou a chlebem.", null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 100);
                addItem(Ft, F.VEJCE, 50);
                addItem(Ft, F.VAJECNY_BILEK, 30);
                addItem(Ft, F.MADELAND_FITNESS_10P_MADETA, 25);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chleba s lučinou a vínem", null, null, null);

                addItem(Ft, F.HROZNOVE_VINO, 120);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.MANDLE, 8);

                Ft = addFood(Dt, T.LUNCH, "Vepřový plátek s dušenou mrkví", "Okořeněnou vepřovou panenku krátce orestujeme na olivovém oleji, podlijeme vodou a dusíme do změknutí. Podáváme s dušenou mrkví a vařenou rýží natural.", null, null);

                addItem(Ft, F.VEPROVA_PANENKA, 120);
                addItem(Ft, F.RYZE_NATURAL, 100);
                addItem(Ft, F.MRKEV, 150);
                addItem(Ft, F.OLIVOVY_OLEJ, 14);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Pečivo s lučinou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 60);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 35);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.DINNER, "Krevety na česneku", "Uvaříme krevety (cca 15min.). Vložíme do rozpáleného oleje a lehce osmahneme. Mezitím si nachystáme protlačený česnek, který přidáme ke krevetám. Podáváme s chlebem a rajčatovým salátem.", null, null);

                addItem(Ft, F.KREVETY, 160);
                addItem(Ft, F.RAJCATA, 150);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 7);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 95);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.OCET_BALZAMICO);

                //DAY p7 in v2
                Dt = addDay(diet, "Day 7");
                Ft = addFood(Dt, T.BREAKFAST, "Pečivo s lučinou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 90);
                addItem(Ft, F.LUCINA_LINIE, 30);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 45);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.KESU_ORISKY, 22);

                Ft = addFood(Dt, T.MORNING_SNACK, "Knackebrot s vínem", null, null, null);

                addItem(Ft, F.HROZNOVE_VINO, 150);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.LUCINA_LINIE, 25);
                addItem(Ft, F.SEMINKA, 12);

                Ft = addFood(Dt, T.LUNCH, "Pohanka s kuřecími kousky", "Pokrájené a okořeněné kuřecí maso orestujeme společně se zeleninou na rozpáleném oleji. Podáváme s vařenou pohankou.", true, null);

                addItem(Ft, F.KURECI_PRSA, 120);
                addItem(Ft, F.POHANKA, 85);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 18);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Sportness tyčinka s jogurtem", null, null, null);

                addItem(Ft, F.BILY_JOGURT_3_P, 150);
                addItem(Ft, F.PROTEINOVA_TYCINKA_SPORTNESS_27P_DM_DROGERIE_BANAN, 45);
                addItem(Ft, F.OVESNE_VLOCKY, 15);

                Ft = addFood(Dt, T.DINNER, "Zeleninové lečo", "Zeleninu osmahneme lehce na oleji, podlijeme vodou, přikryjeme pokličkou a chvíli podusíme. Na závěr přidáme pokrájenou šunku, šmakouna a dochutíme solí a pepřem. Podáváme s chlebem (lze připravit jako salát).", true, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 100);
                addItem(Ft, F.CERSTVA_ZELENINA, 350);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 60);
                addItem(Ft, F.SMAKOUN_ORIGINAL, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 6);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);

                //DAY p8 in v2
                Dt = addDay(diet, "Day 8");
                Ft = addFood(Dt, T.BREAKFAST, "Sladký kuskus", "Kuskus a kakao zalijeme vroucí vodou a necháme dojít v misce pod talířem, aby změkl kuskus. Nakonec přidáme zbytek ingrediencí.", null, null);

                addItem(Ft, F.CELOZRNNY_KUSKUS, 70);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.KESU_ORISKY, 15);
                addItem(Ft, F.AVOKADO, 50);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chleba s banánem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 60);
                addItem(Ft, F.LUCINA_LINIE, 15);
                addItem(Ft, F.BANAN, 100);
                addItem(Ft, F.KESU_ORISKY, 10);

                Ft = addFood(Dt, T.LUNCH, "Kari bulgur", "Na rozpáleném oleji osmahneme nakrájenou cibuli, přidáme sůl, pepř, rozmixovanou zeleninu, rozinky a po prohřátí přilijeme vodu. Do směsi nasypeme bulgur, který vaříme do změknutí. Ke konci přidáme zeleninové koření, trochu kari a posypeme nastrouhaným sýrem.", true, null);

                addItem(Ft, F.BULGUR, 90);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.ROZINKY, 5);
                addItem(Ft, F.MADELAND_LIGHT_30_P, 50);
                addItem(Ft, F.NIZKOTUCNY_TVAROH, 70);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);
                addSpice(Ft, S.PALIVA_PAPRIKA);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CHILLI);
                addSpice(Ft, S.KARI_KORENI);
                addSpice(Ft, S.ZELENINOVE_KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Salát se sýrem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 90);
                addItem(Ft, F.NIVA_MATADOR, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.MANDLE, 8);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);

                Ft = addFood(Dt, T.DINNER, "Zeleninová čočka", "Uvaříme červenou čočku do měkka (cca 12 minut). Po vychladnutí ji smícháme s pokrájenou zeleninou. Krůtí maso osmahneme na oleji. Přidáme k čočce, okořeníme a podáváme s knackebrotem.", null, null);

                addItem(Ft, F.CERVENA_COCKA, 35);
                addItem(Ft, F.KRUTI_PRSA, 90);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.OLIVOVY_OLEJ, 8);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY p9 in v2
                Dt = addDay(diet, "Day 9");
                Ft = addFood(Dt, T.BREAKFAST, "Bageta s lučinou a sýrem", null, null, null);

                addItem(Ft, F.CELOZRNNA_BAGETA, 95);
                addItem(Ft, F.LUCINA_LINIE, 25);
                addItem(Ft, F.MADELAND_LIGHT_30_P, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.MANDLE, 20);

                Ft = addFood(Dt, T.MORNING_SNACK, "Knackebrot s hruškou", null, null, null);

                addItem(Ft, F.HRUSKA, 180);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SEMINKA, 8);

                Ft = addFood(Dt, T.LUNCH, "Rizoto s tuňákem", "Na oleji orestujeme nadrobno nakrájenou cibuli, rýži a zeleninu. Podlijeme vodou a dusíme do měkka. Nakonec vmícháme slitého tuňáka a dokořeníme dle chuti.", true, null);

                addItem(Ft, F.RYZE_NATURAL, 80);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 110);
                addItem(Ft, F.OLIVOVY_OLEJ, 20);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s hruškou", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.HRUSKA, 100);
                addItem(Ft, F.OVESNE_VLOCKY, 15);
                addItem(Ft, F.JAVOROVY_SIRUP, 10);
                addItem(Ft, F.MANDLE, 10);
                addItem(Ft, F.PSENICNE_OTRUBY, 3);
                addItem(Ft, F.PSENICNE_KLICKY, 2);

                Ft = addFood(Dt, T.DINNER, "Seitan steak", "Seitan okořeníme dle chuti a krátce orestujeme na olivovém oleji. Podáváme se zeleninovým salátem a chlebem.", null, null);

                addItem(Ft, F.SEITAN_NATURAL_SUNFOOD, 110);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 8);
                addSpice(Ft, S.HORCICE);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.BYLINKY);


                //DAY p10 in v2
                Dt = addDay(diet, "Day 10");
                Ft = addFood(Dt, T.BREAKFAST, "Pečivo s lučinou, šmakounem a avokádem", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 100);
                addItem(Ft, F.LUCINA_LINIE, 30);
                addItem(Ft, F.SMAKOUN_MEXICO_PROTEIN_FOODS, 70);
                addItem(Ft, F.AVOKADO, 50);
                addItem(Ft, F.KESU_ORISKY, 8);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chleba s lučinou a pomerančem", null, null, null);

                addItem(Ft, F.POMERANC, 200);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 75);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SEMINKA, 7);

                Ft = addFood(Dt, T.LUNCH, "Těstoviny se sušenými rajčaty", "Těstoviny uvaříme v osolené vodě a ke konci přidáme rajčata, aby změkla. Zeleninu rozmixujeme najemno, ochutíme zeleninovým kořením, špetkou cukru a chvíli v kastrolku povaříme. Kuřecí maso nakrájíme na kousky a osmahneme na oleji. Vše smícháme, přidáme bazalku a nakonec posypeme strouhaným parmezánem.", null, null);

                addItem(Ft, F.CELOZRNNE_TESTOVINY, 80);
                addItem(Ft, F.KURECI_PRSA, 100);
                addItem(Ft, F.PARMEZAN_30P, 10);
                addItem(Ft, F.RAJCATA, 200);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 15);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.ZELENINOVE_KORENI);
                addSpice(Ft, S.CUKR);
                addSpice(Ft, S.BAZALKA);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Pečivo s lučinou a sýrem", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 55);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SYR_TVRDY_30P, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.KESU_ORISKY, 6);

                Ft = addFood(Dt, T.DINNER, "Salát se šunkou a sýrem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 90);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 50);
                addItem(Ft, F.MADELAND_FITNESS_10P_MADETA, 40);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 120);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);

                //DAY p11 in v2
                Dt = addDay(diet, "Day 11");
                Ft = addFood(Dt, T.BREAKFAST, "Pečivo s nivou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 100);
                addItem(Ft, F.MASLO, 10);
                addItem(Ft, F.NIVA_MATADOR, 35);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.MANDLE, 12);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb s avokádem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SUSENE_MERUNKY_ALNATURA, 40);
                addItem(Ft, F.AVOKADO, 20);

                Ft = addFood(Dt, T.LUNCH, "Filet s bramborem", "Filet osolíme, opepříme a osmahneme z obou stran na oleji. Podáváme s vařenými brambory a zeleninovým salátem.", null, null);

                addItem(Ft, F.RYBI_FILETY, 150);
                addItem(Ft, F.BRAMBORY, 350);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 18);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Caprese", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 90);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 50);
                addItem(Ft, F.RAJCATA, 200);
                addItem(Ft, F.AVOKADO, 20);
                addSpice(Ft, S.BAZALKA);
                addSpice(Ft, S.OCET_BALZAMICO);

                Ft = addFood(Dt, T.DINNER, "Omeleta se šunkou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 75);
                addItem(Ft, F.VEJCE, 50);
                addItem(Ft, F.VAJECNY_BILEK, 30);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 80);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.SUL);

                //DAY p12 in v2
                Dt = addDay(diet, "Day 12");
                Ft = addFood(Dt, T.BREAKFAST, "Vločky se šmakounem a avokádem", "Vločky a kakao zalijeme vroucí vodou a necháme chvíli nabobtnat. Poté přidáme nastrouhaného šmakouna a zbytek ingrediencí.", null, null);

                addItem(Ft, F.OVESNE_VLOCKY, 60);
                addItem(Ft, F.SMAKOUN_MERUNKOVY_DESSERT_PROTEIN_FOODS, 60);
                addItem(Ft, F.ROZINKY, 15);
                addItem(Ft, F.KESU_ORISKY, 20);
                addItem(Ft, F.AVOKADO, 40);
                addItem(Ft, F.PSENICNE_KLICKY, 5);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);
                addSpice(Ft, S.KAKAO);
                addSpice(Ft, S.SKORICE);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb s lučinou a hruškou", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 80);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.HRUSKA, 100);
                addItem(Ft, F.KESU_ORISKY, 8);

                Ft = addFood(Dt, T.LUNCH, "Kuřecí steak s kuskusem", "Pórek nakrájíme na kolečka a lehce osmahneme na oleji. Přidáme okořeněné kuřecí maso a dusíme společně s pórkem a rajčaty do změknutí. Podáváme s vařeným kuskusem.", null, null);

                addItem(Ft, F.KURECI_PRSA, 120);
                addItem(Ft, F.RAJCATA, 150);
                addItem(Ft, F.POREK, 50);
                addItem(Ft, F.CELOZRNNY_KUSKUS, 85);
                addItem(Ft, F.OLIVOVY_OLEJ, 18);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.OCET_BALZAMICO);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chléb s lučinou", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 85);
                addItem(Ft, F.LUCINA_LINIE, 25);
                addItem(Ft, F.MADELAND_FITNESS_10P_MADETA, 25);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.KESU_ORISKY, 8);

                Ft = addFood(Dt, T.DINNER, "Losos se zeleninou", null, null, null);

                addItem(Ft, F.LOSOS_SE_ZELENINOU_NEKTON_WELNESS, 170);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.CELOZRNNE_PECIVO, 50);
                addItem(Ft, F.SEMINKA, 5);
                addItem(Ft, F.NIZKOTUCNY_TVAROH, 100);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KAKAO);
                addSpice(Ft, S.SKORICE);

                //DAY p13 in v2
                Dt = addDay(diet, "Day 13");
                Ft = addFood(Dt, T.BREAKFAST, "Pečivo s cottage a nivou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 95);
                addItem(Ft, F.COTTAGE, 50);
                addItem(Ft, F.NIVA_MATADOR, 20);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.MANDLE, 25);

                Ft = addFood(Dt, T.MORNING_SNACK, "Pečivo s lučinou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 75);
                addItem(Ft, F.LUCINA_LINIE, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);

                Ft = addFood(Dt, T.LUNCH, "Krůtí špíz s rýží", "Na špejli střídavě napichujeme kousky krůtího masa a zeleniny. Z oleje a oblíbeného koření uděláme marinádu, kterou špíz potřeme a poté upečeme v troubě. Podáváme s vařenou rýží.", true, null);

                addItem(Ft, F.KRUTI_PRSA, 110);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.RYZE_NATURAL, 80);
                addItem(Ft, F.OLIVOVY_OLEJ, 20);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Knackebrot s tyčinkou a oříšky", null, null, null);

                addItem(Ft, F.PROTEINOVA_TYCINKA_SPORTNESS_32P_DM_DROGERIE_COOKIES, 45);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 25);
                addItem(Ft, F.KESU_ORISKY, 12);

                Ft = addFood(Dt, T.DINNER, "Pečený pstruh se salátem", "Rybu vložíme na plech, posypeme kmínem, obložíme směsí koření, bylinek, zeleniny, pokapeme lehce olejem a pečeme v předehřáté troubě do zlatova. V průběhu pečení můžeme rybu obrátit, aby se dobře propekla. Podáváme s vařeným kuskusem a zeleninovým salátem.", true, null);

                addItem(Ft, F.CELOZRNNY_KUSKUS, 60);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.PSTRUH_CERSTVY, 120);
                addItem(Ft, F.OLIVOVY_OLEJ, 6);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                //DAY p14 in v2
                Dt = addDay(diet, "Day 14");
                Ft = addFood(Dt, T.BREAKFAST, "Pečivo s lučinou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 95);
                addItem(Ft, F.LUCINA_LINIE, 30);
                addItem(Ft, F.MADELAND_LIGHT_30_P, 25);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.KESU_ORISKY, 22);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb s lučinou a mandarinkou", null, null, null);

                addItem(Ft, F.MANDARINKA, 170);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.KESU_ORISKY, 8);

                Ft = addFood(Dt, T.LUNCH, "Celozrnné palačinky s ovocem", "Z mouky (voda dvojnásobné množství než mouka), vejce a trochy vody připravíme těsto a pečeme na rozpáleném oleji. Hotové palačinky posypeme skořicí a plníme tvarohem rozšlehaným s jogurtem a rozmačkaným ovocem.", null, null);

                addItem(Ft, F.CELOZRNNA_MOUKA, 80);
                addItem(Ft, F.NIZKOTUCNY_TVAROH, 50);
                addItem(Ft, F.JOGURT_BILY_JIHOCESKY_MADETA_VE_SKLE, 200);
                addItem(Ft, F.VEJCE, 50);
                addItem(Ft, F.OVOCE, 150);
                addItem(Ft, F.OLIVOVY_OLEJ, 10);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.SKORICE);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chleba s lučinou a nivou", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 85);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.NIVA_MATADOR, 20);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.KESU_ORISKY, 8);

                Ft = addFood(Dt, T.DINNER, "Restovaná zelenina se sýrem", "Umytý pórek nakrájíme na kolečka a lehce zpěníme na nepřilnavé pánvi. Přidáme utřený česnek, sůl, nakrájenou zeleninu a krátce orestujeme. Nakonec dochutíme octem, kořením a obložíme plátky sýra (necháme chvíli roztéct). Podáváme s chlebem.", true, null);

                addItem(Ft, F.MADELAND_FITNESS_10P_MADETA, 80);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.POREK, 50);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 105);
                addSpice(Ft, S.OCET_BALZAMICO);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                //DAY p15 in v2
                Dt = addDay(diet, "Day 15");
                Ft = addFood(Dt, T.BREAKFAST, "Vločky s tvarohem a banánem", null, null, null);

                addItem(Ft, F.OVESNE_VLOCKY, 55);
                addItem(Ft, F.TVAROH_PLNOTUCNY, 125);
                addItem(Ft, F.BANAN, 120);
                addItem(Ft, F.KESU_ORISKY, 8);

                Ft = addFood(Dt, T.MORNING_SNACK, "Pečivo s lučinou a banánem", null, null, null);

                addItem(Ft, F.BANAN, 90);
                addItem(Ft, F.CELOZRNNE_PECIVO, 50);
                addItem(Ft, F.LUCINA_LINIE, 15);
                addItem(Ft, F.MANDLE, 8);

                Ft = addFood(Dt, T.LUNCH, "Rizoto s krevetami", "Na oleji zpěníme nakrájenou cibuli, přidáme rozetřený česnek a vložíme rozmražené a vodou propláchnuté krevety. Vše chvíli opékáme, osolíme, opepříme, pak přidáme propláchnutou rýži, zeleninu a vodu. Dusíme, až je rýže měkká a voda se odpaří. Na talíři posypeme strouhaným parmezánem.", true, null);

                addItem(Ft, F.RYZE_NATURAL, 100);
                addItem(Ft, F.KREVETY, 100);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.CIBULE, 40);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.PARMEZAN_30P, 30);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s tvarohem a rozinkami", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 80);
                addItem(Ft, F.ROZINKY, 15);
                addItem(Ft, F.OVESNE_VLOCKY, 40);
                addItem(Ft, F.MANDLE, 10);

                Ft = addFood(Dt, T.DINNER, "Bulgur se zeleninou", "Na oleji osmahneme krůtí maso. Přidáme nakrájenou zeleninu a okořeníme. Ke konci smícháme s vařeným bulgurem.", true, null);

                addItem(Ft, F.KRUTI_PRSA, 120);
                addItem(Ft, F.BULGUR, 55);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 9);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);

                //DAY p16 in v2
                Dt = addDay(diet, "Day 16");
                Ft = addFood(Dt, T.BREAKFAST, "Sladký kuskus", null, null, null);

                addItem(Ft, F.CELOZRNNY_KUSKUS, 60);
                addItem(Ft, F.TVAROH_PLNOTUCNY, 125);
                addItem(Ft, F.POMERANC, 120);
                addItem(Ft, F.KESU_ORISKY, 16);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chleba s lučinou a banánem, tyčinka", null, null, null);

                addItem(Ft, F.BANAN, 100);
                addItem(Ft, F.TYCINKA_LIFEBAR_TRESNOVA_LIFEFOOD, 23);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 45);
                addItem(Ft, F.LUCINA_LINIE, 20);

                Ft = addFood(Dt, T.LUNCH, "Zeleninová cizrna s mozzarellou", "Zeleninu nakrájíme na kousky a opečeme na oleji. Vmícháme slitou cizrnu a přidáme kousky mozzarelly, které necháme rozpustit. Přidáme citronovou šťávu, nasekané bylinky, koření a podáváme s chlebem (lze připravit jako salát).", true, null);

                addItem(Ft, F.CIZRNA_VE_SKLE_ALNATURA, 105);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 75);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 120);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.CITRON);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s tvarohem a pomerančem", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.POMERANC, 120);
                addItem(Ft, F.OVESNE_VLOCKY, 30);
                addItem(Ft, F.KESU_ORISKY, 10);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);

                Ft = addFood(Dt, T.DINNER, "Salát s tuňákem a šmakounem", "Šmakouna nakrájíme na kousky, smícháme s čerstvou zeleninou, slitým tuňákem, okořeníme dle chuti a zakapeme olejem. Podáváme s pečivem.", null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 70);
                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 80);
                addItem(Ft, F.SMAKOUN_MEXICO_PROTEIN_FOODS, 70);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 6);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY p17 in v2
                Dt = addDay(diet, "Day 17");
                Ft = addFood(Dt, T.BREAKFAST, "Chleba s lučinou a sýrem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 105);
                addItem(Ft, F.LUCINA_LINIE, 30);
                addItem(Ft, F.EMENTAL_45P, 30);
                addItem(Ft, F.POMERANC, 150);
                addItem(Ft, F.SEMINKA, 18);

                Ft = addFood(Dt, T.MORNING_SNACK, "Dezert s vločkami", null, null, null);

                addItem(Ft, F.SOJOVY_DEZERT_VANILKOVY_ALPROSOYA, 125);
                addItem(Ft, F.OVESNE_VLOCKY, 25);
                addItem(Ft, F.POMERANC, 150);
                addItem(Ft, F.MANDLE, 5);

                Ft = addFood(Dt, T.LUNCH, "Rajčatový bulgur s kuřecími kousky", "Okořeněné kousky kuřecího masa opečeme na oleji. Přilijeme vodu nebo vývar, přisypeme kousky zeleniny a rajčat, osolíme a ochutíme kořením a pár kapkami citrónu. Ke konci do směsi přimícháme bulgur, trochu vody a vaříme do změknutí bulguru.", null, null);

                addItem(Ft, F.KURECI_PRSA, 130);
                addItem(Ft, F.BULGUR, 85);
                addItem(Ft, F.RAJCATA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 18);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.CITRON);
                addSpice(Ft, S.KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Knackebrot s tyčinkou", null, null, null);

                addItem(Ft, F.PROTEINOVA_TYCINKA_SPORTNESS_32P_DM_DROGERIE_COOKIES, 45);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.MANDLE, 8);

                Ft = addFood(Dt, T.DINNER, "Goody řízek se zeleninou", "Goody řízek pokrájíme na kousky a smícháme s čerstvou zeleninou. Nakonec okořeníme dle chuti a podáváme s chlebem.", null, null);

                addItem(Ft, F.GOODY_RIZEK_CHICKEN_STYLE, 120);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 45);
                addItem(Ft, F.SEMINKA, 8);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);



                //DAY p18 in v2
                Dt = addDay(diet, "Day 18");
                Ft = addFood(Dt, T.BREAKFAST, "Pohanková kaše", "Pohankovou krupici stačí spařit vroucí vodou a nechat chvíli nabobtnat. Poté přidáme ostatní ingredience.", null, null);

                addItem(Ft, F.KRUPICE_POHANKOVA, 50);
                addItem(Ft, F.BANAN, 80);
                addItem(Ft, F.TVAROH_PLNOTUCNY, 125);
                addItem(Ft, F.KESU_ORISKY, 12);
                addItem(Ft, F.PSENICNE_KLICKY, 5);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.MORNING_SNACK, "Tyčinka s banánem, chlebem a lučinou", null, null, null);

                addItem(Ft, F.BANAN, 90);
                addItem(Ft, F.TYCINKA_LIFEBAR_TRESNOVA_LIFEFOOD, 23);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 45);
                addItem(Ft, F.LUCINA_LINIE, 20);

                Ft = addFood(Dt, T.LUNCH, "Sýrové těstoviny s brokolicí", "Těstoviny uvaříme a scedíme. Jemně pokrájenou cibuli a kuřecí maso orestujeme na trošce oleje, přidáme nasekaný česnek, spařenou brokolici rozdělenou na růžičky a pokrájená rajčata (i se šťávou). Ochutíme kořením, solí a promícháme (případně přilijeme trochu vařící vody na zředění zeleninové omáčky). Těstoviny promícháme s hotovou omáčkou a posypeme nastrouhaným sýrem.", null, null);

                addItem(Ft, F.CELOZRNNE_TESTOVINY, 90);
                addItem(Ft, F.SYR_TVRDY_30P, 15);
                addItem(Ft, F.KURECI_PRSA, 80);
                addItem(Ft, F.BROKOLICE, 100);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.RAJCATA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 15);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.PROVENSALSKE_KORENI);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CUKR);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s tvarohem", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 80);
                addItem(Ft, F.MANDARINKA, 80);
                addItem(Ft, F.OVESNE_VLOCKY, 45);
                addItem(Ft, F.KESU_ORISKY, 10);

                Ft = addFood(Dt, T.DINNER, "Krůtí kousky se zeleninou", null, true, null);

                addItem(Ft, F.RYZE_NATURAL, 60);
                addItem(Ft, F.KRUTI_PRSA, 110);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 9);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);

                //DAY p19 in v2
                Dt = addDay(diet, "Day 19");
                Ft = addFood(Dt, T.BREAKFAST, "Rohlík s lučinou a sýrem", null, null, null);

                addItem(Ft, F.CELOZRNNY_ROHLIK, 95);
                addItem(Ft, F.LUCINA_LINIE, 25);
                addItem(Ft, F.EMENTAL_45P, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.MANDLE, 15);

                Ft = addFood(Dt, T.MORNING_SNACK, "Dezert s vločky a vínem", null, null, null);

                addItem(Ft, F.SOJOVY_DEZERT_CHOCOLATE_ALPROSOYA, 125);
                addItem(Ft, F.OVESNE_VLOCKY, 25);
                addItem(Ft, F.HROZNOVE_VINO, 110);

                Ft = addFood(Dt, T.LUNCH, "Seitan s pohankou", "Seitan nakrájíme na kousky, osmahneme na oleji a dochutíme kořením dle chuti. K seitanu přidáme nakrájenou čerstvou zeleninu a dusíme do měkka. Podáváme s vařenou pohankou a okurkovým salátem.", true, null);

                addItem(Ft, F.SEITAN_NATURAL_SUNFOOD, 90);
                addItem(Ft, F.CERSTVA_ZELENINA, 300);
                addItem(Ft, F.POHANKA, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 18);
                addSpice(Ft, S.HORCICE);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.CESNEK);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Tvarohová pomazánka", "Tvaroh smícháme s meduňkou pokrájenou na proužky. Přidáme nasekané mandle a ochutíme citrónovou šťávou a solí. Podáváme s chlebem a zeleninovou oblohou.", null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 80);
                addItem(Ft, F.TVAROH_POLOTUCNY, 80);
                addItem(Ft, F.MANDLE, 12);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CITRON);
                addSpice(Ft, S.MEDUNKA);

                Ft = addFood(Dt, T.DINNER, "Filet na česneku", "Okořeněné rybí maso osmahneme na pánvi. Přidáme protlačený česnek, okořeníme a posypeme semínky. Podáváme s chlebem a rajčatovým salátem.", null, null);

                addItem(Ft, F.RYBI_FILETY, 150);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 105);
                addItem(Ft, F.RAJCATA, 250);
                addItem(Ft, F.SEMINKA, 5);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.SUL);

                //DAY p20 in v2
                Dt = addDay(diet, "Day 20");
                Ft = addFood(Dt, T.BREAKFAST, "Bageta s lučinou a hermelínem", null, null, null);

                addItem(Ft, F.CELOZRNNA_BAGETA, 100);
                addItem(Ft, F.LUCINA_LINIE, 25);
                addItem(Ft, F.HERMELIN_FIGURA_SEDLCANSKY, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.SEMINKA, 20);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chleba s lučinou", null, null, null);

                addItem(Ft, F.MANDARINKA, 160);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 80);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.KESU_ORISKY, 8);

                Ft = addFood(Dt, T.LUNCH, "Thajské krevety", "Rozpálíme pánev wok a opražíme na ní curry pastu (asi 1 minutu, dokud nepustí aroma). Na oleji prudce osmahneme krevety. Poté přilijeme kokosové mléko a krátce povaříme. Těsně před koncem přidáme bambusové plátky a případně ochutíme. Hotovou směs podáváme s rýží a zelným salátem.", null, null);

                addItem(Ft, F.LOUPANE_KREVETY, 140);
                addItem(Ft, F.BAMBUS_V_PLECHOVCE, 100);
                addItem(Ft, F.KOKOSOVE_MLEKO, 30);
                addItem(Ft, F.CERVENA_CURRY_PASTA, 25);
                addItem(Ft, F.RYZE_JASMINOVA, 95);
                addItem(Ft, F.ZELI_CINSKE, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 10);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CUKR);
                addSpice(Ft, S.OCET_BALZAMICO);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Knackebrot s jogurtem a tyčinkou", null, null, null);

                addItem(Ft, F.BILY_JOGURT_15_P, 150);
                addItem(Ft, F.PROTEINOVA_TYCINKA_SPORTNESS_32P_DM_DROGERIE_COOKIES, 45);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 15);

                Ft = addFood(Dt, T.DINNER, "Restovaná zelenina", "Okořeněnou zeleninu krátce orestujeme na nepřilnavé pánvi. Poté ji obložíme plátky sýra a nakrájenou šunkou. Podáváme s chlebem (lze připravit jako salát).", true, null);

                addItem(Ft, F.CERSTVA_ZELENINA, 300);
                addItem(Ft, F.SYR_TVRDY_30P, 60);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 30);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 100);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY p21 in v2
                Dt = addDay(diet, "Day 21");
                Ft = addFood(Dt, T.BREAKFAST, "Bageta s lučinou a sýrem", null, null, null);

                addItem(Ft, F.CELOZRNNA_BAGETA, 95);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.EMENTAL_45P, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.MANDLE, 15);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chleba s lučinou a meruňky", null, null, null);

                addItem(Ft, F.SUSENE_MERUNKY_ALNATURA, 35);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addItem(Ft, F.LUCINA_LINIE, 15);
                addItem(Ft, F.KESU_ORISKY, 10);

                Ft = addFood(Dt, T.LUNCH, "Kuřecí stehna s pohankou", "Okořeněná kuřecí stehna krátce opečeme na oleji a podáváme s vařenou pohankou a zeleninovým salátem.", true, null);

                addItem(Ft, F.KURECI_PRSA, 120);
                addItem(Ft, F.POHANKA, 85);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 15);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Pečivo s lučinou a šunkou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 60);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 30);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.KESU_ORISKY, 12);

                Ft = addFood(Dt, T.DINNER, "Zapečená zelenina", "Zeleninu nakrájíme, poté ji přendáme do zapékací mísy, posypeme nastrouhaným šmakounem a mozzarellou, kořením a chvíli zapékáme. Podáváme s chlebem.", true, null);

                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.SMAKOUN_ORIGINAL, 70);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 100);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 100);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.KORENI);

                //DAY p22 in v2
                Dt = addDay(diet, "Day 22");
                Ft = addFood(Dt, T.BREAKFAST, "Kaše s tvarohem a banánem", null, null, null);

                addItem(Ft, F.KRUPICE_POHANKOVA, 50);
                addItem(Ft, F.BANAN, 60);
                addItem(Ft, F.TVAROH_PLNOTUCNY, 125);
                addItem(Ft, F.KESU_ORISKY, 12);
                addItem(Ft, F.PSENICNE_KLICKY, 5);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.MORNING_SNACK, "Rohlík s lučinou a banánem", null, null, null);

                addItem(Ft, F.CELOZRNNY_ROHLIK, 55);
                addItem(Ft, F.LUCINA_LINIE, 15);
                addItem(Ft, F.BANAN, 80);
                addItem(Ft, F.SEMINKA, 8);

                Ft = addFood(Dt, T.LUNCH, "Zdravý segedínský guláš", "Na oleji osmažíme nakrájenou cibuli, osolíme, opepříme a ke konci zasypeme mletou paprikou. Přidáme nakrájené kostky masa, chvilku restujeme a zalijeme horkou vodou. Vaříme do změknutí, zahustíme moukou, povaříme a cca 3 minuty před koncem přidáme kysané zelí. Dochutíme podle potřeby a podáváme s pečivem.", null, null);

                addItem(Ft, F.VEPROVA_PANENKA, 90);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.ZELI_KYSANE_ALNATURA, 200);
                addItem(Ft, F.CELOZRNNA_MOUKA, 30);
                addItem(Ft, F.CELOZRNNE_PECIVO, 100);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.MLETA_CERVENA_PAPRIKA);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Špenátový salát s lososem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 85);
                addItem(Ft, F.UZENY_LOSOS_NORSKE_PLATKY, 35);
                addItem(Ft, F.SPENATOVE_LISTY, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.KESU_ORISKY, 10);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                Ft = addFood(Dt, T.DINNER, "Krůtí steak s bramborem", null, null, null);

                addItem(Ft, F.KRUTI_PRSA, 120);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.BRAMBORY, 250);
                addItem(Ft, F.OLIVOVY_OLEJ, 8);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY p23 in v2
                Dt = addDay(diet, "Day 23");
                Ft = addFood(Dt, T.BREAKFAST, "Vločky s tvarohem a pomerančem", null, null, null);

                addItem(Ft, F.OVESNE_VLOCKY, 80);
                addItem(Ft, F.TVAROH_PLNOTUCNY, 100);
                addItem(Ft, F.POMERANC, 100);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.MORNING_SNACK, "Mrkvový salát", null, null, null);

                addItem(Ft, F.MRKEV, 150);
                addItem(Ft, F.POMERANC, 150);
                addItem(Ft, F.SEMINKA, 10);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 50);
                addItem(Ft, F.LUCINA_LINIE, 15);
                addSpice(Ft, S.CITRON);

                Ft = addFood(Dt, T.LUNCH, "Mořská štika s bulgurem", "Rybí maso potřeme marinádou z oleje, sójové omáčky, chilli koření, soli, citrónové šťávy a kořenící směsí na ryby podle chuti. Rybu potřeme marinádou a zapečeme v troubě cca 20 minut. Bulgur uvaříme do měkka ve slané vodě a podáváme s hotovou rybou a čerstvým salátem.", null, null);

                addItem(Ft, F.STIKA, 120);
                addItem(Ft, F.BULGUR, 85);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 18);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SOJOVA_OMACKA);
                addSpice(Ft, S.CHILLI);
                addSpice(Ft, S.CITRON);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s tvarohem a pomerančem", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.POMERANC, 100);
                addItem(Ft, F.OVESNE_VLOCKY, 30);
                addItem(Ft, F.JAVOROVY_SIRUP, 10);
                addItem(Ft, F.SEMINKA, 8);

                Ft = addFood(Dt, T.DINNER, "Chléb se šmakounem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 95);
                addItem(Ft, F.COTTAGE_LIGHT_LINESSA_3P, 150);
                addItem(Ft, F.SMAKOUN_HLIVA_USTRICNA_PROTEIN_FOODS, 40);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.KESU_ORISKY, 10);

                //DAY p24 in v2
                Dt = addDay(diet, "Day 24");
                Ft = addFood(Dt, T.BREAKFAST, "Pečivo s lučinou a mozarellou", null, null, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 95);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.SEMINKA, 22);

                Ft = addFood(Dt, T.MORNING_SNACK, "Knackebrot s lučinou a vínem", null, null, null);

                addItem(Ft, F.HROZNOVE_VINO, 160);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SEMINKA, 8);

                Ft = addFood(Dt, T.LUNCH, "Zdravý hovězí hamburger", "Maso nakrájíme na drobné kousky nebo nameleme. Smícháme s částí nakrájené cibule, přidáme sůl, pepř, hořčici, česnek, rozmačkanou cizrnu (nebo cizrnu dáme jen do salátu). Vytvarujeme karbanátek a upečeme v troubě na pečicím papíru. Na oleji orestujeme zbytek cibule, přidáme sůl, pepř, špetku cukru, tymián a zastříkneme vinným octem. Do přepůleného pečiva dáme kečup, zeleninovou oblohu podle chuti, karbanátek s čepicí karamelizované cibule. Podáváme se salátem.", null, null);

                addItem(Ft, F.HOVEZI_ROSTENA, 100);
                addItem(Ft, F.CIZRNA_VE_SKLE_ALNATURA, 50);
                addItem(Ft, F.CIBULE, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.KECUP, 10);
                addItem(Ft, F.CELOZRNNA_BULKA, 110);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.HORCICE);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.TYMIAN);
                addSpice(Ft, S.SPETKA_CUKRU);
                addSpice(Ft, S.VINNY_OCET);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chléb s lučinou a sýrem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 80);
                addItem(Ft, F.LUCINA_LINIE, 25);
                addItem(Ft, F.SYR_TVRDY_30P, 35);
                addItem(Ft, F.HROZNOVE_VINO, 50);

                Ft = addFood(Dt, T.DINNER, "Polníčkový salát", null, null, null);

                addItem(Ft, F.POLNICEK, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 70);
                addItem(Ft, F.SMAKOUN_HLIVA_USTRICNA_PROTEIN_FOODS, 50);
                addItem(Ft, F.CELOZRNNE_PECIVO, 75);
                addItem(Ft, F.OLIVOVY_OLEJ, 8);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.OCET_BALZAMICO);

                //DAY p25 in v2
                Dt = addDay(diet, "Day 25");
                Ft = addFood(Dt, T.BREAKFAST, "Slaná pohanka", "Pohankovou krupici spaříme vroucí vodou a necháme chvíli nabobtnat. Poté okořeníme dle chuti, přidáme pokrájený hermelín (necháme chvíli roztát), posypeme mandlemi a podáváme s čerstvou zeleninou.", null, null);

                addItem(Ft, F.KRUPICE_POHANKOVA, 80);
                addItem(Ft, F.HERMELIN_FIGURA_SEDLCANSKY, 45);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.MANDLE, 22);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb s hruškou", null, null, null);

                addItem(Ft, F.HRUSKA, 180);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 50);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SEMINKA, 8);

                Ft = addFood(Dt, T.LUNCH, "Špagety", "Na oleji orestujeme nakrájenou šunku. Zálivku připravíme z trochy vody, jemně nasekané petrželky, utřeného česneku, soli a pepře. Špagety uvaříme v osolené vodě. Než jsou špagety uvařené, dáme znovu na oheň pánev se šunkou, vmícháme připravenou zálivku a dusíme. Nakonec přimícháme hotové špagety, posypeme nastrouhaným parmezánem a podáváme se zeleninovým salátem.", null, null);

                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 40);
                addItem(Ft, F.CELOZRNNE_SPAGETY, 95);
                addItem(Ft, F.PARMEZAN_30P, 20);
                addItem(Ft, F.OLIVOVY_OLEJ, 14);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.PETRZEL);
                addSpice(Ft, S.CESNEK);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s jogurtem a hruškou", null, null, null);

                addItem(Ft, F.JOGURT_BILY_JIHOCESKY_MADETA_VE_SKLE, 200);
                addItem(Ft, F.PSENICNE_OTRUBY, 3);
                addItem(Ft, F.PSENICNE_KLICKY, 2);
                addItem(Ft, F.OVESNE_VLOCKY, 25);
                addItem(Ft, F.HRUSKA, 90);

                Ft = addFood(Dt, T.DINNER, "Čočkový salát", "Uvaříme červenou čočku do měkka (cca 12 minut). Šmakouna osmahneme na oleji. Po vychladnutí smícháme s čerstvou zeleninou, zalijeme jogurtem a dokořeníme dle chuti.", null, null);

                addItem(Ft, F.CERVENA_COCKA, 50);
                addItem(Ft, F.SMAKOUN_ORIGINAL, 130);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 120);
                addItem(Ft, F.OLIVOVY_OLEJ, 5);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 10);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);

                //DAY p26 in v2
                Dt = addDay(diet, "Day 26");
                Ft = addFood(Dt, T.BREAKFAST, "Vločky s tvarohem a vínem", null, null, null);

                addItem(Ft, F.OVESNE_VLOCKY, 70);
                addItem(Ft, F.TVAROH_PLNOTUCNY, 100);
                addItem(Ft, F.HROZNOVE_VINO, 60);
                addItem(Ft, F.KESU_ORISKY, 10);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addItem(Ft, F.PSENICNE_KLICKY, 3);
                addItem(Ft, F.PSENICNE_OTRUBY, 2);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb s lučinou a vínem", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.HROZNOVE_VINO, 110);
                addItem(Ft, F.KESU_ORISKY, 10);

                Ft = addFood(Dt, T.LUNCH, "Rýže s kuřecím masem", "Kuřecí maso ochutíme pepřem, solí a opečeme na oleji asi 5 minut. Přidáme česnek, kari, rajčata, rozinky a pomalu přivedeme do varu. Pod pokličkou dusíme cca 10 minut (podle potřeby přilijeme vodu), občas zamícháme. Směs promícháme s vařenou rýží.", null, null);

                addItem(Ft, F.KURECI_PRSA, 150);
                addItem(Ft, F.RYZE_NATURAL, 90);
                addItem(Ft, F.RAJCATA, 200);
                addItem(Ft, F.ROZINKY, 15);
                addItem(Ft, F.OLIVOVY_OLEJ, 18);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KARI_KORENI);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.PEPR);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s tvarohem a meruňky", null, null, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 90);
                addItem(Ft, F.OVESNE_VLOCKY, 30);
                addItem(Ft, F.JAVOROVY_SIRUP, 10);
                addItem(Ft, F.SUSENE_MERUNKY_ALNATURA, 20);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.DINNER, "Salát s tuňákem", null, null, null);

                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 100);
                addItem(Ft, F.CERSTVA_ZELENINA, 300);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 105);
                addItem(Ft, F.OLIVOVY_OLEJ, 8);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);

                //DAY p27 in v2
                Dt = addDay(diet, "Day 27");
                Ft = addFood(Dt, T.BREAKFAST, "Bageta s cottage", null, null, null);

                addItem(Ft, F.CELOZRNNA_BAGETA, 90);
                addItem(Ft, F.COTTAGE, 100);
                addItem(Ft, F.RAJCATA, 150);
                addItem(Ft, F.MANDLE, 25);

                Ft = addFood(Dt, T.MORNING_SNACK, "Knackebrot s lučinou a banánem", null, null, null);

                addItem(Ft, F.BANAN, 130);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SEMINKA, 8);

                Ft = addFood(Dt, T.LUNCH, "Pikantní mexické fazole", "Fazole namočíme na 12h do vody, poté vodu slijeme a v hrnci je uvaříme do měkka se špetkou saturejky nebo mořské řasy (pro snížení nadýmání). Na trošce oleje orestujeme pokrájenou zeleninu, kterou dochutíme bylinkami nebo zeleninovým kořením (lze přidat i lžičku chilli pasty). Až rajčata pustí šťávu, přimícháme uvařené fazole a hotovou směs posypeme nastrouhaným sýrem.", true, null);

                addItem(Ft, F.FAZOLE_ADZUKI, 80);
                addItem(Ft, F.RAJCATA, 100);
                addItem(Ft, F.PAPRIKY, 50);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.SYR_TVRDY_30P, 30);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 80);
                addItem(Ft, F.OLIVOVY_OLEJ, 16);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.OREGANO);
                addSpice(Ft, S.KMIN);
                addSpice(Ft, S.CHILLI);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.PETRZEL);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Vločky s jogurtem a šmakounem", null, null, null);

                addItem(Ft, F.BILY_JOGURT_3_P, 150);
                addItem(Ft, F.OVESNE_VLOCKY, 50);
                addItem(Ft, F.SMAKOUN_MERUNKOVY_DESSERT_PROTEIN_FOODS, 20);
                addItem(Ft, F.MANDLE, 5);

                Ft = addFood(Dt, T.DINNER, "Krevetový salát", "Krevety uvaříme ve slané vodě. Z jogurtu, soli a česneku připravíme dresink. Čerstvou zeleninu nakrájíme, zakapeme olejem, přidáme krevety a promícháme s jogurtovým dresinkem. Servírujeme s žitným chlebem.", null, null);

                addItem(Ft, F.KREVETY, 150);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.OLIVOVY_OLEJ, 5);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 60);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 100);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.KORENI);

                //DAY p28 in v2
                Dt = addDay(diet, "Day 28");
                Ft = addFood(Dt, T.BREAKFAST, "Pohanková kaše", "Pohankovou krupici stačí spařit vroucí vodou a nechat chvíli nabobtnat. Poté přidáme nastrouhaného šmakouna a zbytek ingrediencí.", null, null);

                addItem(Ft, F.KRUPICE_POHANKOVA, 55);
                addItem(Ft, F.SMAKOUN_MERUNKOVY_DESSERT_PROTEIN_FOODS, 80);
                addItem(Ft, F.ROZINKY, 10);
                addItem(Ft, F.MANDLE, 35);
                addItem(Ft, F.POMERANC, 80);
                addSpice(Ft, S.KAKAO);
                addSpice(Ft, S.SKORICE);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléba s lučinou a pomerančem", null, null, null);

                addItem(Ft, F.POMERANC, 200);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 65);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.SEMINKA, 8);

                Ft = addFood(Dt, T.LUNCH, "Špenátový kuskus s tuňákem", "Na oleji osmahneme nakrájenou cibuli, poté přidáme steak z tuňáka a rozmražené špenátové listy, které osolíme a opepříme. Kuskus uvaříme v zeleninovém vývaru nebo s bujónem bez glutamátu do měkka. Do teplého kuskusu vmícháme špenátovou směs, přidáme tuňáka a dokořeníme dle chuti.", null, null);

                addItem(Ft, F.KUSKUS_CELOZRNNY, 90);
                addItem(Ft, F.TUNAK_STEAK, 80);
                addItem(Ft, F.SPENATOVE_LISTY_MRAZENE_BONDUELLE, 50);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.ZELENINOVE_KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chleba s lučinou a mozarellou", null, null, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 90);
                addItem(Ft, F.LUCINA_LINIE, 20);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 40);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.MANDLE, 5);

                Ft = addFood(Dt, T.DINNER, "Kuřecí maso s bulgurem", "Bulgur vaříme ve slané vodě do změknutí. Maso pokrájíme na malé nudličky, zeleninu na stejně velké kousky. Maso osmahneme na oleji. Přidáme pokrájenou zeleninu, osolíme a nakonec zakapeme šťávou z citrónu. Restujeme (či dusíme) do změknutí masa. Podáváme s vařeným bulgurem.", true, null);

                addItem(Ft, F.KURECI_PRSA, 100);
                addItem(Ft, F.BULGUR, 55);
                addItem(Ft, F.ZELI_CINSKE, 90);
                addItem(Ft, F.CIBULE, 30);
                addItem(Ft, F.MRKEV, 30);
                addItem(Ft, F.BROKOLICE, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 7);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.CITRON);


                //DO NOT ALTER
                data.DIETs.Add(diet);
                data.SaveChanges();
            }
        }

        public static void createHonzaDiet()
        {
            using (var data = new SmartRegimenEntities())
            {
                //DO NOT ALTER
                DIET_DAY Dt;
                FOOD Ft;


                //DIET SAMPLE
                DIET diet = new DIET();
                diet.TEXT_DESC = addText("Reduction Diet");
                diet.PERSON = addPerson("Jan", "Hnízdil");
                diet.TEXT_NAME = addText("Eat Well Gain Diet");

                /*
                
                //DAY X
                Dt = addDay(diet);
                Ft = addFood(Dt, T.BREAKFAST, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);

                Ft = addFood(Dt, T.MORNING_SNACK, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);

                Ft = addFood(Dt, T.LUNCH, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);

                Ft = addFood(Dt, T.DINNER, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);
                
                 */

                //DAY 1
                Dt = addDay(diet, "Day 1");
                Ft = addFood(Dt, T.BREAKFAST, "Vločky s tvarohem", null, false, null);

                addItem(Ft, F.OVESNE_VLOCKY, 35);
                addItem(Ft, F.TVAROH_POLOTUCNY, 125);
                addItem(Ft, F.KEFIROVE_MLEKO, 50);
                addItem(Ft, F.SEMINKA, 20);
                addItem(Ft, F.BANAN, 90);

                Ft = addFood(Dt, T.MORNING_SNACK, "Banán s čokoládou", null, false, null);

                addItem(Ft, F.BANAN, 110);
                addItem(Ft, F.HRUBOZRNNY_CHLEB, 55);
                addItem(Ft, F.COKOLADA_HORKA, 10);

                Ft = addFood(Dt, T.LUNCH, "Kuskus na cibulce", "Cibuli a mraženou kukuřici orestujeme na oleji, osolíme, opepříme a promícháme s kuskusem uvařeným v mírně slané vodě. Kousky rybího masa okořeníme, orestujeme na zbytku oleje do měkka a teplé je vmícháme do kuskusu. Dle chuti dokořeníme a podáváme se zeleninovým salátem.", true, null);

                addItem(Ft, F.FILE_TRESKA, 140);
                addItem(Ft, F.CELOZRNNY_KUSKUS, 80);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.KUKURICE_JEMNA, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
     
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.GRILOVACI_KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Tvaroh s banánem", null, false, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 80);
                addItem(Ft, F.BANAN, 50);
                addItem(Ft, F.OVESNE_VLOCKY, 35);
                addItem(Ft, F.LISKOVE_ORECHY, 5);

                Ft = addFood(Dt, T.DINNER, "Salát s tuňákem a šmakounem", "Šmakouna nakrájíme na kousky, smícháme s čerstvou zeleninou a slitým tuňákem. Okořeníme dle chuti a zakápneme olejem. Podáváme s chlebem.", false, null);
                
                addItem(Ft, F.HRUBOZRNNY_CHLEB, 85);
                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 80);
                addItem(Ft, F.SMAKOUN_MEXICO_PROTEIN_FOODS , 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.OLIVOVY_OLEJ, 5);
                addSpice(Ft, S.SUL);

                //DAY 2
                Dt = addDay(diet, "Day 2");
                Ft = addFood(Dt, T.BREAKFAST, "Ovesné vločky s hroznovým vínem", null, false, null);

                addItem(Ft, F.OVESNE_VLOCKY, 55);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.HROZNOVE_VINO, 60);
                addItem(Ft, F.SEMINKA, 16);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);

                Ft = addFood(Dt, T.MORNING_SNACK, "Hroznové víno s tyčinkou a chlebem", null, false, null);

                addItem(Ft, F.HROZNOVE_VINO, 70);
                addItem(Ft, F.TYCINKA_LIFEBAR_FIKOVA_LIFEFOOD, 23);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 55);

                Ft = addFood(Dt, T.LUNCH, "Sýrové těstoviny se špenátem", "Těstoviny uvaříme a scedíme. Jemně pokrájenou cibuli osmažíme na trošce oleje, přidáme nasekaný česnek, spařený špenát a pokrájená rajčata (i se šťávou). Ochutíme kořením a solí (případně přilijeme trochu vařící vody na zředění zeleninové omáčky). Těstoviny promícháme s hotovou omáčkou a posypeme nastrouhaným sýrem.", true, null);

                addItem(Ft, F.CELOZRNNE_TESTOVINY, 85);
                addItem(Ft, F.MADELAND_LIGHT_30_P, 55);
                addItem(Ft, F.SPENATOVE_LISTY, 50);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.RAJCATA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 5);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 100);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CUKR);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Tvaroh s hroznovým vínem", null, false, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.HROZNOVE_VINO, 60);
                addItem(Ft, F.OVESNE_VLOCKY, 20);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);
                addItem(Ft, F.MANDLE, 5);

                Ft = addFood(Dt, T.DINNER, "Cottage se šunkou a hráškem", null, false, null);

                addItem(Ft, F.COTTAGE_LIGHT_LINESSA_3P, 150);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 40);
                addItem(Ft, F.HRASEK_ZELENY, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.HRUBOZRNNY_CHLEB, 75);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);

                //DAY 3
                Dt = addDay(diet, "Day 3");
                Ft = addFood(Dt, T.BREAKFAST, "Sladký kuskus", "Kuskus zalijeme vroucí vodou a necháme dojít v misce pod talířem, aby změkl. Nakonec přidáme zbytek ingrediencí.", false, null);

                addItem(Ft, F.CELOZRNNY_KUSKUS, 50);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.MANDLE, 25);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addItem(Ft, F.MANDARINKA, 60);
                addSpice(Ft, S.SKORICE);

                Ft = addFood(Dt, T.MORNING_SNACK, "Tyčinka s knackebrotem", null, false, null);

                addItem(Ft, F.TYCINKA_LIFEBAR_FIKOVA_LIFEFOOD, 23);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.MANDARINKA, 130);


                Ft = addFood(Dt, T.LUNCH, "Sendvič s cizrnovým salátem", "Chleba obložíme plátky šunky. Salát připravíme z nakrájené zeleniny, slité cizrny, dochutíme kořením, octem balzamico a zakápneme olejem.", false, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 125);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 60);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.CIZRNA_VE_SKLE_ALNATURA, 105);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addSpice(Ft, S.OCET_BALZAMICO);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Proteinová tyčinka s knackebrotem", null, false, null);

                addItem(Ft, F.PROTEINOVA_TYCINKA_SPORTNESS_32P_DM_DROGERIE_COOKIES, 45);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 25);
                addItem(Ft, F.SEMINKA, 5);


                Ft = addFood(Dt, T.DINNER, "Rybičková pomazánka", "Okurku a cibuli nakrájíme najemno, smícháme s jogurtem, slitým tuňákem, hořčicí a dochutíme kořením. Podáváme s chlebem a zeleninovým salátem.", false, null);

                addItem(Ft, F.HRUBOZRNNY_CHLEB, 85);
                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 90);
                addItem(Ft, F.CIBULE, 20);
                addItem(Ft, F.HORCICE, 20);
                addItem(Ft, F.BILY_JOGURT_15_P, 150);
                addItem(Ft, F.SLADKOKYSELA_OKURKA, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 100);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);

                //DAY 4
                Dt = addDay(diet, "Day 4");
                Ft = addFood(Dt, T.BREAKFAST, "Pohanková kaše", "Pohankovou krupici stačí spařit vroucí vodou a nechat chvíli nabobtnat. Poté přidáme ostatní ingredience.", false, null);

                addItem(Ft, F.KRUPICE_POHANKOVA, 50);
                addItem(Ft, F.BANAN, 60);
                addItem(Ft, F.TVAROH_OVOFIT, 140);
                addItem(Ft, F.MANDLE, 25);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.MORNING_SNACK, "Šťáva s chlebem", null, false, null);

                addItem(Ft, F.STAVA_S_KOUSKY_OVOCE_RIO_FRESH, 200);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 50);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.LUNCH, "Pečené kuřecí řízky", "Kuřecí prsa osolíme a potřeme utřeným česnekem. Okořeníme dle chuti a vložíme do olejem vymazaného pekáče. Přidáme zbytek česneku nakrájeného na plátky a upečeme v troubě doměkka. Podáváme s vařeným kuskusem a zeleninovým salátem.", true, null);

                addItem(Ft, F.KURECI_PRSA, 120);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.CELOZRNNY_KUSKUS, 80);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 100);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Tvaroh s banánem", null, false, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.KEFIROVE_MLEKO, 50);
                addItem(Ft, F.BANAN, 60);
                addItem(Ft, F.OVESNE_VLOCKY, 20);
                addItem(Ft, F.SEMINKA, 5);

                Ft = addFood(Dt, T.DINNER, "Chléb se šmakounem", "Chléb obložíme na plátky pokrájeným šmakounem (zbytek lze přidat do salátu). Salát připravíme z čerstvé zeleniny a nadrobno pokrájené šunky. Dochutíme kořením, zalijeme bílým jogurtem a posypeme semínky.", null, null);

                addItem(Ft, F.HRUBOZRNNY_CHLEB, 90);
                addItem(Ft, F.SMAKOUN_ORIGINAL, 100);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 50);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 60);
                addItem(Ft, F.SEMINKA, 6);

                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);

                //DAY 5
                Dt = addDay(diet, "Day 5");
                Ft = addFood(Dt, T.BREAKFAST, "Ovesné vločky s kiwi", null, false, null);

                addItem(Ft, F.OVESNE_VLOCKY, 55);
                addItem(Ft, F.TVAROH_POLOTUCNY, 50);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 120);
                addItem(Ft, F.KIWI, 100);
                addItem(Ft, F.SEMINKA, 18);

                Ft = addFood(Dt, T.MORNING_SNACK, "Kiwi s hrubozrnným chlebem", null, false, null);

                addItem(Ft, F.KIWI, 150);
                addItem(Ft, F.HRUBOZRNNY_CHLEB, 80);
                addItem(Ft, F.MANDLE, 8);

                Ft = addFood(Dt, T.LUNCH, "Hovězí maso s fazolkami", "Na oleji necháme zesklovatět cibuli, poté přidáme fazolové lusky, vše osolíme, opepříme a chvilku podusíme (ke konci přidáme lisovaný česnek). Hovězí maso okořeníme a orestujeme do měkka. Podáváme s vařenou rýží a zeleninovou oblohou.", true, null);

                addItem(Ft, F.HOVEZI_SVICKOVA, 140);
                addItem(Ft, F.FAZOLOVE_LUSKY_MRAZENE_BONDUELLE, 100);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 7);
                addItem(Ft, F.RYZE_NATURAL, 80);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.CHILLI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chléb se zeleninovým tvarohem", null, false, null);

                addItem(Ft, F.HRUBOZRNNY_CHLEB, 75);
                addItem(Ft, F.SVACINKA_ZELENINOVY_TVAROH_MILKO, 40);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 40);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.SEMINKA, 7);

                Ft = addFood(Dt, T.DINNER, "Salát s tofu", null, false, null);

                addItem(Ft, F.MARINOVANE_TOFU_SUNFOOD, 200);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 10);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY 6
                Dt = addDay(diet, "Day 6");
                Ft = addFood(Dt, T.BREAKFAST, "Ovesné vločky s tvarohem a pomerančem", null, false, null);

                addItem(Ft, F.OVESNE_VLOCKY, 50);
                addItem(Ft, F.TVAROH_OVOFIT, 140);
                addItem(Ft, F.SEMINKA, 26);
                addItem(Ft, F.POMERANC, 120);

                Ft = addFood(Dt, T.MORNING_SNACK, "Pomeranč s chlebem a oříšky", null, false, null);

                addItem(Ft, F.POMERANC, 200);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 50);
                addItem(Ft, F.LISKOVE_ORECHY, 8);

                Ft = addFood(Dt, T.LUNCH, "Krůtí plátek s dušenou mrkví", "Okořeněná krůtí prsa krátce orestujeme na olivovém oleji a podáváme s dušenou mrkví a vařenou rýží natural.", true, null);

                addItem(Ft, F.KRUTI_PRSA, 100);
                addItem(Ft, F.RYZE_NATURAL, 85);
                addItem(Ft, F.MRKEV, 150);
                addItem(Ft, F.OLIVOVY_OLEJ, 15);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chléb se zeleninovým tvarohem a mozzarellou", null, false, null);

                addItem(Ft, F.HRUBOZRNNY_CHLEB, 80);
                addItem(Ft, F.SVACINKA_ZELENINOVY_TVAROH_MILKO, 50);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 35);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);


                Ft = addFood(Dt, T.DINNER, "Salát se sýrem a šunkou", "Zeleninu nakrájíme a okořeníme. Poté ji obložíme plátky sýra a nakrájenou šunkou. Podáváme s chlebem.", false, null);

                addItem(Ft, F.CERSTVA_ZELENINA, 300);
                addItem(Ft, F.TVRDY_SYR_20P, 60);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 40);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY 7
                Dt = addDay(diet, "Day 7");
                Ft = addFood(Dt, T.BREAKFAST, "Pohanková kaše", "Pohankovou krupici stačí spařit vroucí vodou a nechat chvíli nabobtnat. Poté přidáme ostatní ingredience.", false, null);

                addItem(Ft, F.KRUPICE_POHANKOVA, 40);
                addItem(Ft, F.HROZNOVE_VINO, 100);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 120);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.LISKOVE_ORECHY, 12);
                addSpice(Ft, S.SKORICE);

                Ft = addFood(Dt, T.MORNING_SNACK, "Hroznové víno s knackebrotem", null, false, null);

                addItem(Ft, F.HROZNOVE_VINO, 130);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.SEMINKA, 14);

                Ft = addFood(Dt, T.LUNCH, "Pohanka s kuřecími kousky", "Pokrájené okořeněné kuřecí maso orestujeme společně se zeleninou na rozpáleném oleji a poté promícháme s vařenou pohankou.", true, null);

                addItem(Ft, F.KURECI_PRSA, 130);
                addItem(Ft, F.POHANKA, 90);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Proteinová tyčinka s knackebrotem", null, false, null);

                addItem(Ft, F.PROTEINOVA_TYCINKA_SPORTNESS_27P_DM_DROGERIE_BANAN, 45);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 25);

                Ft = addFood(Dt, T.DINNER, "Šmakoun se zeleninou", "Zeleninu nakrájíme a okořeníme dle chuti. Přidáme pokrájené tvarůžky, šmakouna, zakápneme olejem a promícháme. Podáváme s chlebem.", false, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 75);
                addItem(Ft, F.CERSTVA_ZELENINA, 300);
                addItem(Ft, F.OLOMOUCKE_TVARUZKY, 50);
                addItem(Ft, F.SMAKOUN_ORIGINAL, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 6);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);

                //DAY 8
                Dt = addDay(diet, "Day 8");
                Ft = addFood(Dt, T.BREAKFAST, "Sladký kuskus", "Kuskus a kakao zalijeme vroucí vodou a necháme dojít v misce pod talířem, aby změkl kuskus. Nakonec přidáme zbytek ingrediencí.", false, null);

                addItem(Ft, F.CELOZRNNY_KUSKUS, 50);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.LISKOVE_ORECHY, 16);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);
                addItem(Ft, F.KIWI, 120);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb s kiwi a čokoládou", null, false, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addItem(Ft, F.KIWI, 130);
                addItem(Ft, F.COKOLADA_HORKA, 10);

                Ft = addFood(Dt, T.LUNCH, "Kari bulgur", "Na rozpáleném oleji osmahneme nakrájenou cibuli, přidáme sůl, pepř, rozmixovanou zeleninu, rozinky a po prohřátí přilijeme vodu. Do směsi nasypeme bulgur, který vaříme do změknutí. Ke konci přidáme zeleninové koření, trochu kari a posypeme nastrouhaným sýrem.", true, null);

                addItem(Ft, F.BULGUR, 70);
                addItem(Ft, F.CIBULE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 12);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.ROZINKY, 10);
                addItem(Ft, F.MADELAND_FITNESS_10P_MADETA, 35);
                addItem(Ft, F.NIZKOTUCNY_TVAROH, 100);
                addSpice(Ft, S.PALIVA_PAPRIKA);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CHILLI);
                addSpice(Ft, S.KARI_KORENI);
                addSpice(Ft, S.ZELENINOVE_KORENI);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Salát se šmakounem", null, false, null);

                addItem(Ft, F.HRUBOZRNNY_CHLEB, 90);
                addItem(Ft, F.MADELAND_LIGHT_30_P, 30);
                addItem(Ft, F.SMAKOUN_ORIGINAL, 20);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);

                Ft = addFood(Dt, T.DINNER, "Rukolový salát", null, false, null);

                addItem(Ft, F.RUKOLA, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 80);
                addItem(Ft, F.SMAKOUN_HLIVA_USTRICNA_PROTEIN_FOODS, 40);
                addItem(Ft, F.HRUBOZRNNY_CHLEB, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 5);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.OCET_BALZAMICO);

                //DAY 9
                Dt = addDay(diet, "Day 9");
                Ft = addFood(Dt, T.BREAKFAST, "Ovesné vločky s hruškou", null, false, null);

                addItem(Ft, F.OVESNE_VLOCKY, 50);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.SEMINKA, 20);
                addItem(Ft, F.HRUSKA, 100);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);

                Ft = addFood(Dt, T.MORNING_SNACK, "Hruška s knackebrotem", null, false, null);

                addItem(Ft, F.HRUSKA, 140);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.LUNCH, "Rizoto s tuňákem", "Na oleji orestujeme nadrobno nakrájenou cibuli, rýži a mraženou zeleninu. Podlijeme vodou a dusíme do měkka. Nakonec vmícháme slitého tuňáka a dokořeníme dle chuti.", true, null);

                addItem(Ft, F.RYZE_NATURAL, 80);
                addItem(Ft, F.MRAZENA_ZELENINA_MRKEV_KUKURICE_HRASEK, 150);
                addItem(Ft, F.CIBULE, 40);
                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 90);
                addItem(Ft, F.OLIVOVY_OLEJ, 14);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Tvaroh s hruškou", null, false, null);

                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.HRUSKA, 90);
                addItem(Ft, F.PSENICNE_OTRUBY, 10);
                addItem(Ft, F.OVESNE_VLOCKY, 10);
                addItem(Ft, F.COKOLADA_HORKA, 5);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);

                Ft = addFood(Dt, T.DINNER, "Goody řízek", "Goody řízek pokrájíme na kousky a smícháme s čerstvou zeleninou. Okořeníme a podáváme s knackebrotem.", false, null);

                addItem(Ft, F.GOODY_RIZEK_CHICKEN_STYLE, 140);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 15);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.BYLINKY);

                //DAY 10
                Dt = addDay(diet, "Day 10");
                Ft = addFood(Dt, T.BREAKFAST, "Sladký bulgur", "Bulgur uvaříme do měkka, poté přidáme zbytek ingrediencí.", false, null);

                addItem(Ft, F.BULGUR, 30);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 120);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.BANAN, 100);
                addItem(Ft, F.SEMINKA, 15);
                addSpice(Ft, S.SKORICE);

                Ft = addFood(Dt, T.MORNING_SNACK, "Banán s knackebrotem", null, false, null);

                addItem(Ft, F.BANAN, 100);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 30);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.LUNCH, "Kuřecí maso s rýží", "Na rozpáleném oleji orestujeme nadrobno nakrájenou cibuli, na kousky pokrájené maso a rýži. Potom přidáme rozmraženou zeleninu. Podlijeme vodou a dusíme, dokud rýže a maso nebude měkké. Nakonec dochutíme solí a kořením.", true, null);

                addItem(Ft, F.RYZE_NATURAL, 80);
                addItem(Ft, F.MRAZENA_ZELENINA_MRKEV_KUKURICE_HRASEK, 150);
                addItem(Ft, F.CIBULE, 40);
                addItem(Ft, F.OLIVOVY_OLEJ, 14);
                addItem(Ft, F.KURECI_PRSA, 110);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chléb s olomouckými tvarůžky", null, false, null);

                addItem(Ft, F.HRUBOZRNNY_CHLEB, 80);
                addItem(Ft, F.OLOMOUCKE_TVARUZKY, 35);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.LISKOVE_ORECHY, 10);

                Ft = addFood(Dt, T.DINNER, "Salát se šunkou a jogurtem", null, false, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 100);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.JOGURT_ACTIVIA_BILA_DANONE, 120);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY 11
                Dt = addDay(diet, "Day 11");
                Ft = addFood(Dt, T.BREAKFAST, "Pohanková kaše", "Pohankovou krupici stačí spařit vroucí vodou a nechat chvíli bobtnat, poté přidáme ostatní ingredience.", false, null);

                addItem(Ft, F.KRUPICE_POHANKOVA, 55);
                addItem(Ft, F.GRAPEFRUIT, 100);
                addItem(Ft, F.LISKOVE_ORECHY, 14);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.PSENICNE_OTRUBY, 10);
                addSpice(Ft, S.SKORICE);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb se sušenými meruňkami", null, false, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 55);
                addItem(Ft, F.SUSENE_MERUNKY_ALNATURA, 40);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.LUNCH, "Filet s bulgurem", "Filet osolíme, opepříme a osmahneme z obou stran na oleji. Podáváme s vařeným bulgurem a zeleninovým salátem.", false, null);

                addItem(Ft, F.RYBI_FILETY, 110);
                addItem(Ft, F.BULGUR, 90);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.OLIVOVY_OLEJ, 15);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.BYLINKY);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Caprese", null, false, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 70);
                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 30);
                addItem(Ft, F.SUNKA_NAD_90P_MASA_DO_3P_TUKU, 20);
                addItem(Ft, F.RAJCATA, 150);
                addItem(Ft, F.MANDLE, 5);
                addSpice(Ft, S.BAZALKA);
                addSpice(Ft, S.OCET_BALZAMICO);

                Ft = addFood(Dt, T.DINNER, "Kuřecí polévka s nudlemi", "Na nudličky nakrájená kuřecí prsa krátce orestujeme na oleji, poté zalijeme vodou, přidáme cibuli, pepř, nové koření a nať, osolíme a uvedeme do varu. Přidáme kořenovou zeleninu a vaříme do změknutí. Ke konci přidáme nudle. Polévku posypeme nakrájenou pažitkou a podáváme.", true, null);

                addItem(Ft, F.KURECI_PRSA, 120);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.CELOZRNNE_NUDLE, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 5);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY 12
                Dt = addDay(diet, "Day 12");
                Ft = addFood(Dt, T.BREAKFAST, "Ovesné vločky s hrouškou", "Vločky a kakao zalijeme vroucí vodou a necháme chvíli nabobtnat. Poté přidáme nastrouhaného šmakouna a zbytek ingrediencí.", false, null);

                addItem(Ft, F.SMAKOUN_MERUNKOVY_DESSERT_PROTEIN_FOODS, 100);
                addItem(Ft, F.OVESNE_VLOCKY, 50);
                addItem(Ft, F.ROZINKY, 10);
                addItem(Ft, F.LISKOVE_ORECHY, 18);
                addItem(Ft, F.HRUSKA, 60);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                Ft = addFood(Dt, T.MORNING_SNACK, "Chléb s hruškou", null, false, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 50);
                addItem(Ft, F.HRUSKA, 150);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.LUNCH, "Kuřecí plátek s rýží a rajčaty", "Pórek nakrájíme na kolečka a lehce osmahneme na oleji. Přidáme okořeněné kuřecí maso a dusíme společně s pórkem a rajčaty do změknutí. Podáváme s vařenou rýží a orestovaným (či podušeným) hráškem.", true, null);

                addItem(Ft, F.KURECI_PRSA, 110);
                addItem(Ft, F.RAJCATA, 150);
                addItem(Ft, F.POREK, 50);
                addItem(Ft, F.RYZE_NATURAL, 80);
                addItem(Ft, F.HRASEK_ZELENY, 50);
                addItem(Ft, F.OLIVOVY_OLEJ, 14);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.OCET);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chléb se sýrem a zeleninou", null, false, null);

                addItem(Ft, F.ZITNY_CHLEB_PENAM, 85);
                addItem(Ft, F.MADELAND_FITNESS_10P_MADETA, 35);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);

                Ft = addFood(Dt, T.DINNER, "Losos se zeleninou + dezert", null, false, null);

                addItem(Ft, F.LOSOS_SE_ZELENINOU_NEKTON_WELNESS, 170);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 20);
                addItem(Ft, F.SEMINKA, 5);
                addItem(Ft, F.NIZKOTUCNY_TVAROH, 100);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.KORENI);

                //DAY 13
                Dt = addDay(diet, "Day 13");
                Ft = addFood(Dt, T.BREAKFAST, "Ovesné vločky s mandarinkou", null, false, null);

                addItem(Ft, F.OVESNE_VLOCKY, 50);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.MANDARINKA, 100);
                addItem(Ft, F.SEMINKA, 22);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);

                Ft = addFood(Dt, T.MORNING_SNACK, "Celozrnné pečivo se zeleninou", null, false, null);

                addItem(Ft, F.CELOZRNNE_PECIVO, 65);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addItem(Ft, F.LISKOVE_ORECHY, 5);

                Ft = addFood(Dt, T.LUNCH, "Zdravý hovězí burger", "Maso nakrájíme na drobné kousky nebo nameleme. Smícháme s částí nakrájené cibule, přidáme sůl, pepř, hořčici, česnek, rozmačkanou cizrnu (nebo cizrnu dáme jen do salátu). Vytvarujeme karbanátek a upečeme v troubě na pečicím papíru. Na oleji orestujeme zbytek cibule, přidáme sůl, pepř, špetku cukru, tymián a zastříkneme vinným octem. Do přepůleného pečiva dáme kečup, zeleninovou oblohu podle chuti, karbanátek s čepicí karamelizované cibule. Podáváme se salátem.", false, null);

                addItem(Ft, F.HOVEZI_ROSTENA, 100);
                addItem(Ft, F.CIZRNA_VE_SKLE_ALNATURA, 50);
                addItem(Ft, F.CIBULE, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 7);
                addItem(Ft, F.KECUP, 10);
                addItem(Ft, F.CELOZRNNA_BULKA, 90);
                addItem(Ft, F.CERSTVA_ZELENINA, 150);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.HORCICE);
                addSpice(Ft, S.CESNEK);
                addSpice(Ft, S.TYMIAN);
                addSpice(Ft, S.CUKR);
                addSpice(Ft, S.VINNY_OCET);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Tyčinka s čokoládou", null, false, null);

                addItem(Ft, F.PROTEINOVA_TYCINKA_SPORTNESS_32P_DM_DROGERIE_COOKIES, 45);
                addItem(Ft, F.KNACKEBROT_ZITNY_RACIO, 15);
                addItem(Ft, F.COKOLADA_HORKA, 10);

                Ft = addFood(Dt, T.DINNER, "Salát s tuňákem", null, false, null);

                addItem(Ft, F.TUNAK_VE_VLASTNI_STAVE, 90);
                addItem(Ft, F.CERSTVA_ZELENINA, 250);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 95);
                addItem(Ft, F.OLIVOVY_OLEJ, 5);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.PEPR);
                addSpice(Ft, S.CESNEK);

                //DAY 14
                Dt = addDay(diet, "Day 14");
                Ft = addFood(Dt, T.BREAKFAST, "Ovesné vločky s banánem", null, false, null);

                addItem(Ft, F.OVESNE_VLOCKY, 50);
                addItem(Ft, F.TVAROH_POLOTUCNY, 100);
                addItem(Ft, F.BANAN, 60);
                addItem(Ft, F.SEMINKA, 18);
                addItem(Ft, F.JAVOROVY_SIRUP, 5);
                addItem(Ft, F.PSENICNE_OTRUBY, 5);

                Ft = addFood(Dt, T.MORNING_SNACK, "Banán s chlebem a semínky", null, false, null);

                addItem(Ft, F.BANAN, 110);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 45);
                addItem(Ft, F.SEMINKA, 10);

                Ft = addFood(Dt, T.LUNCH, "Celozrnné palačinky s ovocem", "Z mouky, vejce a trochy vody připravíme těsto a pečeme na rozpáleném oleji. Hotové palačinky posypeme skořicí a plníme tvarohem rozšlehaným s jogurtem a rozmačkaným ovocem. (Voda dvojnásobné množství než mouka.)", false, null);

                addItem(Ft, F.CELOZRNNA_MOUKA, 70);
                addItem(Ft, F.NIZKOTUCNY_TVAROH, 50);
                addItem(Ft, F.JOGURT_BILY_JIHOCESKY_MADETA_VE_SKLE, 200);
                addItem(Ft, F.VEJCE, 50);
                addItem(Ft, F.OVOCE, 100);
                addItem(Ft, F.OLIVOVY_OLEJ, 5);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.SKORICE);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "Chléb se sýrem a zeleninou", null, false, null);

                addItem(Ft, F.HRUBOZRNNY_CHLEB, 90);
                addItem(Ft, F.MADELAND_FITNESS_10P_MADETA, 35);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);

                Ft = addFood(Dt, T.DINNER, "Salát s mozzarellou + dezert", null, false, null);

                addItem(Ft, F.MOZZARELLA_LIGHT_LINESSA, 50);
                addItem(Ft, F.CERSTVA_ZELENINA, 200);
                addItem(Ft, F.ZITNY_CHLEB_PENAM, 75);
                addItem(Ft, F.NIZKOTUCNY_TVAROH, 125);
                addItem(Ft, F.SEMINKA, 5);
                addSpice(Ft, S.KORENI);
                addSpice(Ft, S.SUL);
                addSpice(Ft, S.SKORICE);
                addSpice(Ft, S.KAKAO);

                //DO NOT ALTER
                data.DIETs.Add(diet);
                data.SaveChanges();
            }
        }

        //THIS IS A TEMPLATE FUNCTION, DO NOT ALTER, COPY, THEN RUN IN Default.aspx.cs
        public static void createTEST()
        {
            using (var data = new SmartRegimenEntities())
            {
                //DO NOT ALTER
                DIET_DAY Dt;
                FOOD Ft;


                //DIET SAMPLE
                DIET diet = new DIET();
                diet.TEXT_DESC = addText("Gain Diet");
                diet.PERSON = addPerson("Šimon", "Hlaváč");
                diet.TEXT_NAME = addText("Eat Well Gain Diet");

                /*
                
                //DAY p in v2
                Dt = addDay(diet);
                Ft = addFood(Dt, T.BREAKFAST, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);

                Ft = addFood(Dt, T.MORNING_SNACK, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);

                Ft = addFood(Dt, T.LUNCH, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);

                Ft = addFood(Dt, T.AFTERNOON_SNACK, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);

                Ft = addFood(Dt, T.DINNER, "", null, null, null);

                addItem(Ft, F., );
                addSpice(Ft, S.);
                
                 */


                //DAY 1
                Dt = addDay(diet, "X");
                Ft = addFood(Dt, T.BREAKFAST, "Snídaně Testová", "Sní bla desc bla", false, 55);

                addItem(Ft, F.BANAN, 150);
                addItem(Ft, F.BILY_JOGURT_15_P, 100);
                addItem(Ft, F.BROKOLICE, 50);

                Ft = addFood(Dt, T.LUNCH, "Oběd Testová", "Sní bla desc bla", false, 55);

                addItem(Ft, F.BULGUR, 75);
                addItem(Ft, F.CELOZRNNA_BULKA, 50);
                addItem(Ft, F.BROKOLICE, 50);
                addSpice(Ft, S.CESNEK);

                Ft = addFood(Dt, T.DINNER, "Večeře Testová", "Sní bla desc bla", false, 55);

                addItem(Ft, F.HRUSKA, 300);
                addItem(Ft, F.FAZOLE_ADZUKI, 20);
                addItem(Ft, F.AVOKADO, 50);
                addSpice(Ft, S.CITRON);



                //DAY 2
                Dt = addDay(diet, "Y");
                Ft = addFood(Dt, T.BREAKFAST, "Snídaně Testová2", "Sní bla desc bla", false, 55);

                addItem(Ft, F.OKURKA, 150);
                addItem(Ft, F.AVOKADO, 20);
                addItem(Ft, F.DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML, 50);

                Ft = addFood(Dt, T.LUNCH, "Oběd Testová2", "Sní bla desc bla", false, 55);

                addItem(Ft, F.ZELI_CINSKE, 75);
                addItem(Ft, F.FAZOLOVE_LUSKY_MRAZENE_BONDUELLE, 500);
                addItem(Ft, F.EMENTAL_45P, 50);
                addSpice(Ft, S.VINNY_OCET);

                Ft = addFood(Dt, T.DINNER, "Večeře Testová2", "Sní bla desc bla", false, 55);

                addItem(Ft, F.GOODY_RIZEK_CHICKEN_STYLE, 100);
                addItem(Ft, F.BILY_JOGURT_3_P, 20);
                addItem(Ft, F.UZENY_LOSOS_NORSKE_PLATKY, 50);
                addSpice(Ft, S.CHILLI);


                //DO NOT ALTER
                data.DIETs.Add(diet);
                data.SaveChanges();
            }
        }

        public static void createPrices()
        {
            using (var data = new SmartRegimenEntities())
            {
                // AVOKADO
                addPrice(data,6062, 34.9, 150.0, 0.7, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012692885");
                // BAMBUS_V_PLECHOVCE
                addPrice(data,4750, 35, 227, 0.61, 2, "https://www.rohlik.cz/hledat/avok%C3%A1do?productPopup=723821-exotic-food-bambusove-vyhonky-platky");
                // BANAN
                addPrice(data,6063, 32.9, 1000, 0.75, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012690126");
                // BILY_JOGURT_15_P
                addPrice(data,6535, 8.9, 150, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000348527");
                // BILY_JOGURT_3_P
                addPrice(data,6521, 8.9, 150, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000348527");
                // BRAMBORY
                addPrice(data,2889, 14.9, 1000, 0.9, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012781145");
                // BROKOLICE
                addPrice(data,2890, 24.9, 500, 0.9, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012974837");
                // BULGUR
                addPrice(data,1, 45.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120313323");
                // CELOZRNNA_MOUKA
                addPrice(data,8249, 26.9, 1000, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001009386759");
                // CELOZRNNE_PECIVO
                addPrice(data,4962, 21.9, 450, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130532307");
                // CELOZRNNE_TESTOVINY
                addPrice(data,5906, 49.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130293474");
                // CERSTVA_ZELENINA
                addPrice(data,2898, 42, 1000, 0.9, 1, "http://nakup.itesco.cz/cs-CZ/Product/BrowseProducts?taxonomyId=Cat00000019");
                // CERVENA_COCKA
                addPrice(data,2050, 35.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120290815");
                // CERVENA_CURRY_PASTA
                addPrice(data,2413, 23.9, 50, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001015404577");
                // CIBULE
                addPrice(data,2902, 16.9, 1000, 0.9, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012691574");
                // CIZRNA_VE_SKLE_ALNATURA
                addPrice(data,4674, 17.9, 400, 0.6, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120607391");
                // COKOLADA_HORKA
                addPrice(data,3709, 28.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130547872");
                // COTTAGE
                addPrice(data,6766, 28.9, 150, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001011249325");
                // COTTAGE_LIGHT_LINESSA_3P
                addPrice(data,6819, 28.9, 150, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001011249325");
                // DZUS_POMERANCOVY_100P_BEZ_CUKRU_150_ML
                addPrice(data,3429, 31.9, 1000, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001017808533");
                // EMENTAL_45P
                addPrice(data,6949, 32.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001018382148");
                // FAZOLE_ADZUKI
                addPrice(data,2065, 48.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000430932");
                // FAZOLOVE_LUSKY_MRAZENE_BONDUELLE
                addPrice(data,3502, 28.9, 400, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130270017");
                // FILE_TRESKA
                addPrice(data,1669, 69.9, 400, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000423750");
                // GOODY_RIZEK_CHICKEN_STYLE
                addPrice(data,977, 54.9, 145, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130337648");
                // GRAPEFRUIT
                addPrice(data,6075, 39.9, 1000, 0.85, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012692816");
                // HERMELIN_FIGURA_SEDLCANSKY
                addPrice(data,6783, 29.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001015853030");
                // HORCICE
                addPrice(data,2290, 6.9, 400, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001018783938");
                // HOVEZI_ROSTENA
                addPrice(data,2172, 35.9, 100, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120883575");
                // HOVEZI_SVICKOVA
                addPrice(data,2140, 79, 100, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120883557");
                // HRASEK_ZELENY
                addPrice(data,3505, 31.9, 400, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130269999");
                // HROZNOVE_VINO
                addPrice(data,6076, 6.99, 100, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012691772");
                // HRUSKA
                addPrice(data,6077, 44.9, 1000, 0.9, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001014284569");
                // JAVOROVY_SIRUP
                addPrice(data,2194, 209, 330, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120321808");
                // JOGURT_ACTIVIA_BILA_DANONE
                addPrice(data,6588, 9.9, 120, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120797164");
                // JOGURT_BILY_JIHOCESKY_MADETA_VE_SKLE
                addPrice(data,6658, 16.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001013209297");
                // KEFIROVE_MLEKO
                addPrice(data,6365, 13.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001007174631");
                // KECUP
                addPrice(data,2324, 42.9, 450, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001015587072");
                // KESU_ORISKY
                addPrice(data,2097, 34.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130553943");
                // KIWI
                addPrice(data,6083, 6.9, 100, 0.9, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012692861");
                // KNACKEBROT_ZITNY_RACIO
                addPrice(data,5237, 39.9, 250, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001011273108");
                // KOKOSOVE_MLEKO
                addPrice(data,2346, 72.9, 400, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000106714");
                // KRUPICE_POHANKOVA
                addPrice(data,78, 64.9, 300, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120855453");
                // KRUTI_PRSA
                addPrice(data,7329, 229.75, 1000, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120544268");
                // KUKURICE_JEMNA
                addPrice(data,3512, 33.9, 400, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001011881525");
                // KURECI_PRSA
                addPrice(data,7330, 188.9, 1000, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120571270");
                // KURECI_STEHNO_BEZ_KUZE
                addPrice(data,7336, 158.9, 1000, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120573982");
                // KUSKUS_CELOZRNNY
                addPrice(data,3, 41.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120493903");
                // LISKOVE_ORECHY
                addPrice(data,2098, 109.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130554235");
                // LOSOS_SE_ZELENINOU_NEKTON_WELNESS
                addPrice(data,1715, 89.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001019431227");
                // LOUPANE_KREVETY
                addPrice(data,1698, 29.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130269433");
                // LUCINA_LINIE
                addPrice(data,6795, 29.9, 120, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012599764");
                // MADELAND_FITNESS_10P_MADETA
                addPrice(data,8336, 32.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120270332");
                // MADELAND_LIGHT_30_P
                addPrice(data,6870, 32.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001015806166");
                // MANDARINKA
                addPrice(data,6090, 44.9, 1000, 0.85, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001013564945");
                // MANDLE
                addPrice(data,2095, 99.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120703375");
                // MARINOVANE_TOFU_SUNFOOD
                addPrice(data,953, 25.9, 180, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120833358");
                // MASLO
                addPrice(data,1935, 45.9, 250, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001018209377");
                // MOZZARELLA_LIGHT_LINESSA
                addPrice(data,6798, 19.9, 125, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120601466");
                // MRAZENA_ZELENINA_MRKEV_KUKURICE_HRASEK
                addPrice(data,3548, 33.9, 400, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000425532");
                // MRKEV
                addPrice(data,2925, 24.9, 1000, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012691581");
                // NIVA_MATADOR
                addPrice(data,6800, 59.9, 220, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001018818609");
                // NIZKOTUCNY_TVAROH
                addPrice(data,7152, 23.9, 250, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000135301");
                // OKURKA
                addPrice(data,2926, 13.9, 300, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001014829975");
                // OLIVOVY_OLEJ
                addPrice(data,1905, 99.9, 1000, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120373389");
                // OLOMOUCKE_TVARUZKY
                addPrice(data,7012, 39.9, 167, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001018297138");
                // OVESNE_VLOCKY
                addPrice(data,30, 27.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000119332");
                // OVOCE
                addPrice(data,6104, 28, 1000, 0.9, 1, "http://nakup.itesco.cz/cs-CZ/Search/List?searchQuery=ovoce&pageNo=1&SortBy=Relevance&Hierarchy=Department%3A11333%3AOvoce");
                // PAPRIKY
                addPrice(data,2927, 49.9, 1000, 0.85, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012702225");
                // PARMEZAN_30P
                addPrice(data,6807, 49.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120535665");
                // PEKINGSKE_ZELI
                addPrice(data,2971, 16.9, 1000, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012691413");
                // POHANKA
                addPrice(data,11, 39.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000094912");
                // POMERANC
                addPrice(data,6108, 36.9, 1000, 0.85, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012691659");
                // POREK
                addPrice(data,2938, 14.9, 333, 0.85, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120587021");
                // PROTEINOVA_TYCINKA_SPORTNESS_27P_DM_DROGERIE_BANAN
                addPrice(data,1591, 16.9, 45, 1, 3, "http://www.zdravapotravina.cz/cukrovinky/sportness-eiwei-32-dm-drogerie-markt");
                // PROTEINOVA_TYCINKA_SPORTNESS_32P_DM_DROGERIE_COOKIES
                addPrice(data,1592, 16.9, 45, 1, 3, "http://www.zdravapotravina.cz/cukrovinky/sportness-eiwei-32-dm-drogerie-markt");
                // PSENICNE_KLICKY
                addPrice(data,2, 12, 200, 1, 3, "http://www.countrylife.cz/klicky-psenicne-200-g-country-life");
                // PSENICNE_OTRUBY
                addPrice(data,7, 9, 200, 1, 3, "http://www.countrylife.cz/otruby-psenicne-200-g-country-life");
                // PSTRUH_CERSTVY
                addPrice(data,1759, 21.9, 100, 0.85, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001018156749");
                // RAJCATA
                addPrice(data,2940, 24.9, 1000, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012690935");
                // ROZINKY
                addPrice(data,6153, 38.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001013256888");
                // POLNICEK
                addPrice(data,2937, 29.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120215382");
                // RUKOLA
                addPrice(data,2948, 29.9, 80, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120892736");
                // RYZE_JASMINOVA
                addPrice(data,7877, 55.9, 1000, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130182686");
                // RYZE_NATURAL
                addPrice(data,7878, 28.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000094561");
                // SEITAN_NATURAL_SUNFOOD
                addPrice(data,1121, 44, 242, 1, 2, "https://www.rohlik.cz/?productPopup=1290675-seitan-natural-trvanlivy");
                // SEMINKA
                addPrice(data,2130, 21.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001016775911");
                // SLADKOKYSELA_OKURKA
                addPrice(data,2975, 46.9, 670, 0.58, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001015525517");
                // SMAKOUN_HLIVA_USTRICNA_PROTEIN_FOODS
                addPrice(data,949, 59.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001019450051");
                // SMAKOUN_MERUNKOVY_DESSERT_PROTEIN_FOODS
                addPrice(data,950, 59.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001019450044");
                // SMAKOUN_MEXICO_PROTEIN_FOODS
                addPrice(data,1140, 59.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120542253");
                // SMAKOUN_ORIGINAL
                addPrice(data,949, 59.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130337717");
                // SMAKOUN_UZENY_PROTEIN_FOODS
                addPrice(data,1145, 59.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130337717");
                // SOJOVY_DEZERT_CHOCOLATE_ALPROSOYA
                addPrice(data,1224, 21.9, 125, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120058210");
                // SOJOVY_DEZERT_VANILKOVY_ALPROSOYA
                addPrice(data,1223, 21.9, 125, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001018812324");
                // SPENATOVE_LISTY_MRAZENE_BONDUELLE
                addPrice(data,3527, 44.9, 450, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001007274751");
                // STAVA_S_KOUSKY_OVOCE_RIO_FRESH
                addPrice(data,3439, 49.9, 750, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120462088");
                // SUNKA_NAD_90P_MASA_DO_3P_TUKU
                addPrice(data,2638, 32.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001019226243");
                // SUSENE_MERUNKY_ALNATURA
                addPrice(data,6148, 59.9, 200, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012675703");
                // SVACINKA_ZELENINOVY_TVAROH_MILKO
                addPrice(data,7143, 15.9, 150, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001009733768");
                // STIKA
                addPrice(data,1812, 82.9, 300, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001019321221");
                // TUNAK_STEAK
                addPrice(data,1852, 49.9, 185, 0.7, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001015612774");
                // TUNAK_VE_VLASTNI_STAVE
                addPrice(data,1661, 169.9, 350, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001019012266");
                // TVAROH_OVOFIT
                addPrice(data,7145, 15.9, 140, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001011103016");
                // TVAROH_PLNOTUCNY
                addPrice(data,7169, 25.9, 250, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000135318");
                // TVAROH_POLOTUCNY
                addPrice(data,7166, 24.9, 250, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000135325");
                // TVRDY_SYR_20P
                addPrice(data,6846, 129.9, 170, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001120692218");
                // TYCINKA_LIFEBAR_FIKOVA_LIFEFOOD
                addPrice(data,4610, 29, 47, 1, 3, "http://eshop.lifefood.cz/index.php?main_page=product_info&cPath=42&products_id=1398");
                // TYCINKA_LIFEBAR_TRESNOVA_LIFEFOOD
                addPrice(data,4610, 29, 47, 1, 3, "http://eshop.lifefood.cz/index.php?main_page=product_info&cPath=42&products_id=1397");
                // UZENY_LOSOS_NORSKE_PLATKY
                addPrice(data,1713, 89.9, 100, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001019431227");
                // VAJECNY_BILEK
                addPrice(data,1894, 22.9, 345, 0.5, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012430203");
                // VEJCE
                addPrice(data,1893, 22.9, 345, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012430203");
                // VEPROVA_PANENKA
                addPrice(data,2138, 226, 1000, 0.95, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001018361471");
                // ZELI_CINSKE
                addPrice(data,2969, 16.9, 1000, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001012691413");
                // ZELI_KYSANE_ALNATURA
                addPrice(data, 4747, 25.9, 500, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001000098354");
                // ZITNY_CHLEB_PENAM
                addPrice(data,5077, 21.9, 450, 1, 1, "http://nakup.itesco.cz/cs-CZ/ProductDetail/ProductDetail/2001130532307");

                data.SaveChanges();
            }
        }

        public static void addItem(FOOD food, int foodItemId, int amount)
        {
            using (var data = new SmartRegimenEntities())
            {
                FOOD_ITEM_IN_FOOD fiif = new FOOD_ITEM_IN_FOOD();
                fiif.FIIF_AMOUNT = amount;
                fiif.FOOD = food;
                fiif.FIIF_FOIT_ID = foodItemId;

                data.FOOD_ITEM_IN_FOOD.Add(fiif);
            }
        }

        public static void addSpice(FOOD food, int spiceId)
        {
            using (var data = new SmartRegimenEntities())
            {
                food.SPICEs.Add(data.SPICEs.Find(spiceId));
            }
        }

        public static FOOD addFood(DIET_DAY dd, int foodType, String name, String desc, bool? coockedVeg, int? duration)
        {
            using (var data = new SmartRegimenEntities())
            {
                FOOD_IN_DIET_DAY fdd = new FOOD_IN_DIET_DAY();
                FOOD food = new FOOD();

                data.FOOD_IN_DIET_DAY.Add(fdd);
                data.FOODs.Add(food);

                fdd.FIDD_FOTY_ID = foodType;
                dd.FOOD_IN_DIET_DAY.Add(fdd);
                food.FOOD_IN_DIET_DAY.Add(fdd);


                if (name != null) food.TEXT_NAME = addText(name);
                if (desc != null) food.TEXT_DESC = addText(desc);
                food.FOOD_DURATION = duration;
                food.FOOD_COOKED_VEG = coockedVeg;

                return food;
            }
        }

        public static DIET_DAY addDay(DIET diet, string name)
        {
            using (var data = new SmartRegimenEntities())
            {
                DIET_DAY dd = new DIET_DAY();
                dd.DIDA_NAME = name;
                diet.DIET_DAY.Add(dd);
                data.DIET_DAY.Add(dd);
                return dd;
            }
        }

        public static TEXT addText(String text)
        {
            using (var data = new SmartRegimenEntities())
            {
                TEXT t = new TEXT();
                t.TEXT_CONTENT = text;
                t.TEXT_LANG_ID = 1;



                data.TEXTs.Add(t);

                return t;
            }
        }

        public static PERSON addPerson(String name, String surname)
        {
            using (var data = new SmartRegimenEntities())
            {
                PERSON person = new PERSON();

                person.PERS_LANG_ID = 1;
                person.PERS_NAME = name;
                person.PERS_SURNAME = surname;


                data.People.Add(person);

                return person;
            }
        }

        public static void addPrice(SmartRegimenEntities data, int FoodItemID, double Cost, double Grams, double Utilization, int store, string link)
        {
                PRICE price = new PRICE();

                price.CURRENCY = data.CURRENCies.Find(1);
                price.FOOD_ITEM = data.FOOD_ITEM.Find(FoodItemID);
                price.STORE = data.STOREs.Find(store);
                price.PRIC_TIMESTAMP = DateTime.Now;
                price.PRIC_COST = Cost / Grams / Utilization;
                price.PRIC_LINK = link;

                data.PRICEs.Add(price);
        }
    }
}