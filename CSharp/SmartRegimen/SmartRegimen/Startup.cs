﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SmartRegimen.Startup))]
namespace SmartRegimen
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
