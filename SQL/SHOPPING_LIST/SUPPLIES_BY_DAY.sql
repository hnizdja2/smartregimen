SELECT 
	FOOD,
	SUM(GRAMS) as GRAMS,
	COOKED_VEG

FROM 

(
	SELECT * FROM [dbo].[GetDailyFood](1) UNION ALL
	SELECT * FROM [dbo].[GetDailyFood](1) UNION ALL

	SELECT * FROM [dbo].[GetDailyFood](2) UNION ALL
	SELECT * FROM [dbo].[GetDailyFood](2) UNION ALL

	SELECT * FROM [dbo].[GetDailyFood](3) UNION ALL
	SELECT * FROM [dbo].[GetDailyFood](3) UNION ALL

	SELECT * FROM [dbo].[GetDailyFood](4) UNION ALL
	SELECT * FROM [dbo].[GetDailyFood](4) UNION ALL

	SELECT * FROM [dbo].[GetDailyFood](5) UNION ALL
	SELECT * FROM [dbo].[GetDailyFood](5)
) F

GROUP BY F.FOOD, COOKED_VEG
ORDER BY F.FOOD
 
--#########################################################

SELECT 
	SPICE,
	COUNT(*) as TIMES
FROM 

(
	SELECT * FROM [dbo].[GetDailySpices](1) UNION ALL
	SELECT * FROM [dbo].[GetDailySpices](1) UNION ALL

	SELECT * FROM [dbo].[GetDailySpices](2) UNION ALL
	SELECT * FROM [dbo].[GetDailySpices](2) UNION ALL

	SELECT * FROM [dbo].[GetDailySpices](3) UNION ALL
	SELECT * FROM [dbo].[GetDailySpices](3) UNION ALL

	SELECT * FROM [dbo].[GetDailySpices](4) UNION ALL
	SELECT * FROM [dbo].[GetDailySpices](4) UNION ALL

	SELECT * FROM [dbo].[GetDailySpices](5) UNION ALL
	SELECT * FROM [dbo].[GetDailySpices](5)
) F

GROUP BY F.SPICE
ORDER BY F.SPICE

--#########################################################

USE [db5834]
GO

/****** Object:  UserDefinedFunction [dbo].[GetDailyFood]    Script Date: 5/4/2015 4:54:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[GetDailyFood]
(	
	-- Add the parameters for the function here
	@input nvarchar(50) 
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
	TXT.TEXT_CONTENT as FOOD,
	FITEMF.FIIF_AMOUNT AS GRAMS,
	COALESCE((CASE WHEN TXT.TEXT_ID = 2997 THEN FOOD.[FOOD_COOKED_VEG] ELSE 0 END),0) AS COOKED_VEG,
	FITEM.FOIT_ENERGY * FITEMF.FIIF_AMOUNT AS Energy, 
    FITEM.FOIT_FAT * FITEMF.FIIF_AMOUNT AS Fat, 
	FITEM.FOIT_PROTEIN * FITEMF.FIIF_AMOUNT AS Protein, 
	FITEM.FOIT_SUGAR * FITEMF.FIIF_AMOUNT AS Sugar

FROM PERSON PERS
JOIN DIET DIET ON DIET.DIET_PERS_ID = PERS.PERS_ID
JOIN DIET_DAY DDAY ON DDAY.DIDA_DIET_ID = DIET.DIET_ID
JOIN FOOD_IN_DIET_DAY FDDAY ON FDDAY.FIDD_DIDA_ID = DDAY.DIDA_ID
JOIN FOOD FOOD ON FOOD.FOOD_ID = FDDAY.FIDD_FOOD_ID
JOIN FOOD_ITEM_IN_FOOD FITEMF ON FITEMF.FIIF_FOOD_ID = FOOD.FOOD_ID
JOIN FOOD_ITEM FITEM ON FITEM.FOIT_ID = FITEMF.FIIF_FOIT_ID
JOIN TEXT TXT ON TXT.TEXT_ID = FITEM.FOIT_NAME_TEXT_ID
WHERE DDAY.DIDA_ID = @input
)




GO







--#########################################################

USE [db5834]
GO

/****** Object:  UserDefinedFunction [dbo].[GetDailySpices]    Script Date: 4/24/2015 10:50:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[GetDailySpices]
(	
	-- Add the parameters for the function here
	@input nvarchar(50) 
)
RETURNS TABLE 
AS
RETURN 
(
SELECT 
	TXT.TEXT_CONTENT as SPICE


FROM PERSON PERS
JOIN DIET DIET ON DIET.DIET_PERS_ID = PERS.PERS_ID
JOIN DIET_DAY DDAY ON DDAY.DIDA_DIET_ID = DIET.DIET_ID
JOIN FOOD_IN_DIET_DAY FDDAY ON FDDAY.FIDD_DIDA_ID = DDAY.DIDA_ID
JOIN FOOD FOOD ON FOOD.FOOD_ID = FDDAY.FIDD_FOOD_ID
JOIN SPICE_IN_FOOD FITEMF ON FITEMF.SPIF_FOOD_ID = FOOD.FOOD_ID
JOIN SPICE FITEM ON FITEM.SPIC_ID = FITEMF.SPIF_SPIC_ID
JOIN TEXT TXT ON TXT.TEXT_ID = FITEM.SPIC_NAME_TEXT_ID
WHERE DDAY.DIDA_ID = @input
)



GO


