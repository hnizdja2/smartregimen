USE [db5834]

SELECT 
	FOOD,
	SUM(GRAMS) as GRAMS,
	COOKED_VEG,
	LINK,
	FOOD + ' ' + CAST(SUM(GRAMS) as NVARCHAR(32)) + 'g'
FROM 

(
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Sn�dane') UNION ALL
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Dopolen� svacina') UNION ALL --
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Odpoledn� svacina') UNION ALL
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Vecere') UNION ALL

	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Sn�dane') UNION ALL
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Dopolen� svacina') UNION ALL --
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Odpoledn� svacina') UNION ALL
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Vecere') UNION ALL

	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Sn�dane') UNION ALL
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Dopolen� svacina') UNION ALL --
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Odpoledn� svacina') UNION ALL
	SELECT * FROM [dbo].[GetFoodSupplies](1, 'Day 24', 'Vecere') 

) F

GROUP BY F.FOOD, COOKED_VEG, LINK
ORDER BY F.FOOD