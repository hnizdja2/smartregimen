/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartregimenminer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import smartregimenminer.crawler.EatWellCrawlController;
import smartregimenminer.crawler.EatWellDataHolder;
import smartregimenminer.crawler.EatWellParser;

/**
 *
 * @author hlavasim
 */
public class SmartRegimenMiner {

    public static int text_id = 100;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        StringBuilder sb = new StringBuilder();

        sb.append("SET IDENTITY_INSERT LANGUAGE ON\r\n");
        sb.append("INSERT INTO LANGUAGE (LANG_ID, LANG_NAME) VALUES (1, \"Česky\");").append("\r\n");
sb.append("SET IDENTITY_INSERT LANGUAGE OFF\r\n");
sb.append("SET IDENTITY_INSERT TEXT ON\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (24,1,\"Alternativy masa a mléka\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (38,1,\"Bonbóny\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (23,1,\"Cereálie, musli, kaše\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (40,1,\"Fast food\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (25,1,\"Fitness, doplňky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (41,1,\"Hotová jídla\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (29,1,\"Houby\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (45,1,\"Chlebíčky, lahůdky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (44,1,\"Instantní slaná\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (33,1,\"Jídla mražená\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (6,1,\"Jogurty\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (3,1,\"Kompoty,přesnídávky, džemy\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (30,1,\"Luštěniny\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (12,1,\"Masné výrobky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (9,1,\"Maso drůbeží\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (10,1,\"Maso hovězí,vepřové\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (11,1,\"Maso ostatní\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (5,1,\"Mléčné kysané\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (8,1,\"Mléčné-ostatní\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (4,1,\"Mléko a smetana\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (21,1,\"Mouky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (34,1,\"Nápoje alko\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (35,1,\"Nápoje nealko\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (22,1,\"Obiloviny + výrobky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (32,1,\"Ochucovadla, přísady\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (43,1,\"Omáčky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (31,1,\"Ořechy, semena\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (1,1,\"Ovoce čerstvé\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (2,1,\"Ovoce sušené\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (16,1,\"Pečivo\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (17,1,\"Pečivo křehké\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (18,1,\"Pečivo sladké\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (42,1,\"Polévky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (26,1,\"Ryby, plody moře\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (20,1,\"Rýže\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (36,1,\"Sladkosti\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (37,1,\"Slané pamlsky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (7,1,\"Sýry\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (19,1,\"Těstoviny\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (28,1,\"Tuky\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (27,1,\"Vejce\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (13,1,\"Zelenina čerstvá\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (15,1,\"Zelenina konzerva\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (14,1,\"Zelenina mražená\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (39,1,\"Zmrzlina\");").append("\r\n");

        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (50,1,\"Snídaně\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (51,1,\"Dopolení svačina\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (52,1,\"Oběd\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (53,1,\"Odpolední svačina\");").append("\r\n");
        sb.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (54,1,\"Večeře\");").append("\r\n");
sb.append("SET IDENTITY_INSERT TEXT OFF\r\n");
sb.append("SET IDENTITY_INSERT CATEGORY ON\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (24,24);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (38,38);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (23,23);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (40,40);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (25,25);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (41,41);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (29,29);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (45,45);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (44,44);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (33,33);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (6,6);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (3,3);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (30,30);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (12,12);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (9,9);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (10,10);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (11,11);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (5,5);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (8,8);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (4,4);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (21,21);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (34,34);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (35,35);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (22,22);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (32,32);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (43,43);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (31,31);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (1,1);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (2,2);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (16,16);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (17,17);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (18,18);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (42,42);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (26,26);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (20,20);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (36,36);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (37,37);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (7,7);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (19,19);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (28,28);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (27,27);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (13,13);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (15,15);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (14,14);").append("\r\n");
        sb.append("INSERT INTO CATEGORY (CATE_ID, CATE_NAME_TEXT_ID) VALUES (39,39);").append("\r\n");
sb.append("SET IDENTITY_INSERT CATEGORY OFF\r\n");
sb.append("SET IDENTITY_INSERT FOOD_TYPE ON\r\n");
        sb.append("INSERT INTO FOOD_TYPE (FOTY_ID, FOTY_NAME_TEXT_ID) VALUES (1,50);").append("\r\n");
        sb.append("INSERT INTO FOOD_TYPE (FOTY_ID, FOTY_NAME_TEXT_ID) VALUES (2,51);").append("\r\n");
        sb.append("INSERT INTO FOOD_TYPE (FOTY_ID, FOTY_NAME_TEXT_ID) VALUES (3,52);").append("\r\n");
        sb.append("INSERT INTO FOOD_TYPE (FOTY_ID, FOTY_NAME_TEXT_ID) VALUES (4,53);").append("\r\n");
        sb.append("INSERT INTO FOOD_TYPE (FOTY_ID, FOTY_NAME_TEXT_ID) VALUES (5,54)").append("\r\n;").append("\r\n");
sb.append("SET IDENTITY_INSERT FOOD_TYPE OFF\r\n");

        EatWellCrawlController.run();

        for (String key : EatWellDataHolder.raw.keySet()) {
            String html = EatWellDataHolder.raw.get(key);
        }

        for (String key : EatWellDataHolder.raw.keySet()) {
            String html = EatWellDataHolder.raw.get(key);
            String json = EatWellParser.getDocument(html);

            EatWellDataHolder.parsed.put(key, json);
        }

        for (String key : EatWellDataHolder.parsed.keySet()) {
            String content = EatWellDataHolder.parsed.get(key);

            sb.append(content);
        }

        sb.append(newSpice("Bazalka"));
        sb.append(newSpice("Ocet balzamico"));
        sb.append(newSpice("Česnek"));
        sb.append(newSpice("Provensálské koření"));
        sb.append(newSpice("Sůl"));
        sb.append(newSpice("Špetka cukru"));
        sb.append(newSpice("Pepř"));
        sb.append(newSpice("Bylinky"));
        sb.append(newSpice("Citronová šťáva"));
        sb.append(newSpice("Koření"));
        sb.append(newSpice("Ocet"));
        sb.append(newSpice("Pálivá paprika"));
        sb.append(newSpice("Chilli"));
        sb.append(newSpice("Kari"));
        sb.append(newSpice("Zeleninové koření"));
        sb.append(newSpice("Skořice"));
        sb.append(newSpice("Kakao"));
        sb.append(newSpice("Sójová omáčka"));
        sb.append(newSpice("Citrón"));
        sb.append(newSpice("Meduňka"));
        sb.append(newSpice("Oregáno"));
        sb.append(newSpice("Kmín"));
        sb.append(newSpice("Petržel"));
        sb.append(newSpice("Mletá červená paprika"));
        sb.append(newSpice("Grilovací koření"));
        sb.append(newSpice("Hořčice"));
        sb.append(newSpice("Tymián"));
        sb.append(newSpice("Vinný ocet"));
        sb.append(newSpice("Kari koření"));
        sb.append(newSpice("Koření na čínu"));
        sb.append(newSpice("Voda"));
        sb.append(newSpice("Olej"));
        sb.append(newSpice("Cukr"));

        /*
         # User
         ################################
         create table USER(
         USER_ID INT AUTO_INCREMENT PRIMARY KEY,
         USER_LANG_ID INT NOT NULL,
         USER_NAME VARCHAR(50) NOT NULL,
         USER_SURNAME VARCHAR(50) NOT NULL,
         FOREIGN KEY (USER_LANG_ID) REFERENCES LANGUAGE(LANG_ID)
         );
        
         /*
         vytvorit hierarchickou strukturu jidel, tu objektove naplnit a pak jen toMySQL();
         */
        File file = new File("./DBFull.sql");
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        FileWriter fw;
        try {
            fw = new FileWriter(file, false);
            fw.write(sb.toString());
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(SmartRegimenMiner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String newSpice(String spice) {
        String spiceSQL = "INSERT INTO SPICE (SPIC_NAME_TEXT_ID) VALUES (" + text_id + ");\r\n";
        String textSQL = "SET IDENTITY_INSERT TEXT ON\r\nINSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (" + text_id + ",1,\"" + spice + "\");\r\nSET IDENTITY_INSERT TEXT OFF\r\n\r\n";
        text_id++;
        return textSQL + spiceSQL;
    }
}
