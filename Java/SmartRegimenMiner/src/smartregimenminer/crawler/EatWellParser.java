/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smartregimenminer.crawler;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.internal.ir.Node;
import jdk.nashorn.internal.parser.JSONParser;
import jdk.nashorn.internal.runtime.Source;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import smartregimenminer.SmartRegimenMiner;

/**
 *
 * @author hlavasim
 */
public class EatWellParser {

    

    public static String getDocument(String html) {

        html = html.replace("'s", "&&&&&s");
        html = html.replace("'S", "&&&&&S");
        html = html.replace("'n", "&&&&&n");
        html = html.replace("'O", "&&&&&O");
        html = html.replace("d'", "d&&&&&");
        html = html.replace("'D", "&&&&&D");

        //System.out.println(html);
        Document htmlDOM = Jsoup.parse(html);
        Elements aDOM = htmlDOM.getElementsByTag("a");

        StringBuilder sbT = new StringBuilder();
        StringBuilder sbF = new StringBuilder();
        
        sbT.append("SET IDENTITY_INSERT TEXT ON\r\n");

        for (Element a : aDOM) {
            String json = a.outerHtml();
            json = json.substring(json.indexOf("{"), json.indexOf("}") + 1);
            json = Jsoup.parse(json).text();
            json = unescapeJava(json);

            //System.out.println(json);
            json = json.replaceAll("&&&&&", "'");

            org.json.simple.parser.JSONParser parser = new org.json.simple.parser.JSONParser();
            JSONObject root = null;
            try {
                root = (JSONObject) parser.parse(json);
            } catch (ParseException ex) {
                Logger.getLogger(EatWellParser.class.getName()).log(Level.SEVERE, null, ex);
            }

            //System.out.println(json);

            String name = ((String) root.get("name")).replaceAll("\"", "");
            Long category_id = (Long) root.get("category_id");
            Double weight = convert((String) root.get("weight"), true) * 1.0D;
            String energy = String.format("%15.5f", convert((String) root.get("energy")) / weight);
            String fat = String.format("%15.5f", convert((String) root.get("fat")) / weight);
            String protein = String.format("%15.5f", convert((String) root.get("protein")) / weight);
            String sugar = String.format("%15.5f", convert((String) root.get("sugar")) / weight);


            sbT.append("INSERT INTO TEXT (TEXT_ID, TEXT_LANG_ID, TEXT_CONTENT) VALUES (").append(SmartRegimenMiner.text_id).append(",1,\"").append(name).append("\");").append("\r\n");
            sbF.append("INSERT INTO FOOD_ITEM (FOIT_NAME_TEXT_ID, FOIT_SUGAR, FOIT_FAT, FOIT_PROTEIN, FOIT_CATE_ID, FOIT_ENERGY) VALUES (").append(SmartRegimenMiner.text_id).append(",").append(sugar).append(",").append(fat).append(",").append(protein).append(",").append(category_id).append(",").append(energy).append(");").append("\r\n").append("\r\n");
            SmartRegimenMiner.text_id++;
        }
        
        sbT.append("SET IDENTITY_INSERT TEXT OFF\r\n");

        return sbT.toString() + sbF.toString();
    }

    public static Double convert(String field) {
        return convert(field, false);
    }

    public static Double convert(String field, Boolean hundretIfNull) {
        Double r = 0D;

        field = field.trim().replaceAll("[^0-9.]", "").replaceAll("\\.\\.", "\\.");

        if (field.startsWith(".")) {
            field = field.substring(1);
        }

        if (field.endsWith(".")) {
            field = field.substring(0, field.length() - 2);
        }

        if (field.length() == 0) {
            return 100D;
        }

        try {
            long parseLong = Long.parseLong(field);
            r = parseLong * 1.0D;
        } catch (Exception ex) {
            r = Double.parseDouble(field);
        }

        return r;
    }

    public static String unescapeJava(String escaped) {
        if (escaped.indexOf("\\u") == -1) {
            return escaped;
        }

        String processed = "";

        int position = escaped.indexOf("\\u");
        while (position != -1) {
            if (position != 0) {
                processed += escaped.substring(0, position);
            }
            String token = escaped.substring(position + 2, position + 6);
            escaped = escaped.substring(position + 6);
            processed += (char) Integer.parseInt(token, 16);
            position = escaped.indexOf("\\u");
        }
        processed += escaped;

        return processed;
    }
}
